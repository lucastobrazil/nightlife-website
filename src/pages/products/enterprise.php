<section id="mmn" class="row text-center"  style="background-image: url('img/what_we_do/products/nightlife_enterprise_bg.jpg'); color: white">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-2">
		<div class="col-lg-8 col-lg-offset-2">
		    <img src="img/icons/manage_my_nightlife_icon.jpg" class="img-responsive img-centered" alt="">
		    <h2>Nightlife Enterprise</h2>
		    <hr class="colored">
		    <h3>Whether you have 1 site or 1,000 sites we help you deliver a consistent brand and message across all of them. </h3>
		</div>
		<div class="col-lg-8 col-lg-offset-2">
		    <p>Our enterprise solution lets you manage your brand's advertisements, insights and controls to help you manage every site from one spot. </p>
			<p>Nightlife Enterprise gives you complete transparency and control across all of your sites ensuring that you deliver a consistent experience.</p>
		</div>
	</div>
</section>