/*!
 * Vitality v1.3.4 (http://themes.startbootstrap.com/vitality-v1.3.4)
 * Copyright 2013-2015 Start Bootstrap Themes
 * To use this theme you must have a license purchased at WrapBootstrap (https://wrapbootstrap.com)
 */

(function($) {
    "use strict"; // Start of use strict

    var showJourneyNav = false; //bool to decide whether or not the journey nav is needed
    
    // Smooth Scrolling: Smooth scrolls to an ID on the current page.
    // To use this feature, add a link on your page that links to an ID, and add the .page-scroll class to the link itself. See the docs for more details.
    $('body').on('click', '.page-scroll', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 950, 'easeInOutQuint', function () {
            window.location.hash = $anchor.attr('href');
            console.log($anchor.attr('href'));
        });
        return false;
    });

    // Separate page scroll for the FAQ nav. These links have their own CSS selectors
    // instead of a conditional inside the function above so that we don't need to
    // check every time a page-scroll link is clicked
    $('body').on('click', '#faq-nav .faq-page-scroll', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 100)
        }, 850, 'easeInOutQuint');
        event.preventDefault();
    });

    // Activates floating label headings for the contact form.
    $("body").on("input propertychange", ".floating-label-form-group", function(e) {
        $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
    }).on("focus", ".floating-label-form-group", function() {
        $(this).addClass("floating-label-form-group-with-focus");
    }).on("blur", ".floating-label-form-group", function() {
        $(this).removeClass("floating-label-form-group-with-focus");
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    // Owl Carousel Settings
    $(".about-carousel").owlCarousel({
        items: 3,
        nav: true,
        pagination: false,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
    });

    $(".portfolio-carousel").owlCarousel({
        items: 1,
        singleItem: true,
        nav: true,
        dots: true,
        lazyLoad: true,
        pagination: false,
        loop: true,
        navRewind: true,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
        autoplay: false,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        autoHeight: false,
        responsive: true,
        mouseDrag: true,
        touchDrag: true,
        transitionStyle: "slide"
    });

    $(".standard-carousel").owlCarousel({
        items: 1,
        singleItem: true,
        nav: true,
        dots: true,
        loop: true,
        lazyLoad: true,
        pagination: false,
        navRewind: true,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
        autoplay: false,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        autoHeight: false,
        responsive: true,
        mouseDrag: true,
        touchDrag: true,
        transitionStyle: "slide"
    });


    $(".portfolio-gallery").owlCarousel({
        items: 3,
    });

    // Magnific Popup jQuery Lightbox Gallery Settings
    $('.gallery-link').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        },
        image: {
            titleSrc: 'title'
        }
    });



    // Formstone Wallpaper - Video Background Settings
    $("header.video").background({
        autoPlay: false,
        source: {
            poster: "img/bg-mobile-fallback.jpg",
            mp4: "mp4/NL_WebsiteBGVideo_20150610_720p24_Web-Ready.mp4",
            webm: "mp4/NL_WebsiteBGVideo_20150610_720p24_Web-Ready.webm"
        }
    });
    $(".fs-background-animated video").attr("id", "wallpaperVideo");

    // Scrollspy plugin for the FAQs page
    $('body').scrollspy({
        target: '#faq-nav',
        offset: 100
    });

    // Lucas A - Added this small preloading script
    // Don't start Wallpaper video until 20% loaded
    var wallpaperVideo = document.getElementById("wallpaperVideo");
    if(wallpaperVideo){

        wallpaperVideo.onprogress = function(){
            var w = 100*(wallpaperVideo.buffered.end(0))/wallpaperVideo.duration;
            var isAlreadyPlaying = false;
            if (w >= 100){
                $("header.video").background("play");
                isAlreadyPlaying = true;
                return;
            }else if(w >= 20){
                if(!isAlreadyPlaying){
                    $("header.video").background("play");
                }
            }
        }
    }
    
    // (function doStuff() {
    //   // Do stuff
    //     //console.log('tifk');
    //     var percent = $('video')[0].buffered.end(0) / 20.065;
    //     console.log(percent +  "% loaded");
    //    setTimeout(doStuff, 1000);
    // }());

    // Temp hack for the login page as it doesn't work yet.
    //$("#login :input").attr('disabled', true);

    $(document).ready(function(){
        // // To handle modals, check if there is a special symbol 
        // // to show what modal should be open
        // // 
        // var hash = window.location.hash;
        // var str = hash.substr(hash.indexOf('#')+1).split('&');
       
        // if(str[1]){
        //     var modalToOpen = "#" + str[1] + "-modal";
        //     console.log(modalToOpen);
        //     $(modalToOpen).modal('toggle');
        // }

        // HTML5 video - make all start at 50% volume
        $(function() {
            $("video").each(function(){ this.volume = 0.2; });
        });

        if(location.hash){
            setTimeout(function(){

               // var $(target) = '#products';

                $('html, body').stop().animate({
                    scrollTop: $(location.hash).offset().top - 50
                }, 950, 'easeInOutQuint'
            )}, 1);
        }
        
    });

    // Scrollspy: Highlights the navigation menu items while scrolling.
    // $('body').scrollspy({
    //     target: '.journey-nav',
    //     offset: 51
    // })

    // Portfolio Filtering Scripts & Hover Effect
    var filterList = {
        init: function() {

            // MixItUp plugin
            // http://mixitup.io
            $('#productsgrid').mixitup({
                targetSelector: '.portfolio',
                filterSelector: '.filter',
                effects: ['fade'],
                easing: 'snap',
                // call the hover effect
                onMixEnd: filterList.hoverEffect()
            });

        },

        hoverEffect: function() {
            // Simple parallax effect
            $('#productsgrid .portfolio').hover(
                function() {
                    $(this).find('.caption').stop().animate({
                        bottom: 0
                    }, 200, 'easeOutQuad');
                    $(this).find('img').stop().animate({
                        top: -20
                    }, 300, 'easeOutQuad');
                },
                function() {
                    $(this).find('.caption').stop().animate({
                        bottom: -62
                    }, 200, 'easeInQuad');
                    $(this).find('img').stop().animate({
                        top: 0
                    }, 300, 'easeOutQuad');
                }
            );
        }
    };

    filterList.init();

    $('.clikko').on('click', function(){
        alert('r');
    });

})(jQuery); // End of use strict

// Load WOW.js on non-touch devices
var isPhoneDevice = "ontouchstart" in document.documentElement;
$(document).ready(function() {
    if (isPhoneDevice) {
        //mobile
    } else {
        //desktop               
        // Initialize WOW.js
        wow = new WOW({
            offset: 50
        })
        wow.init();
    }
});

function setModalVideoSize(vid){
    var aspect = 16/9;
    var windowHeight = $(window).height();
    var windowWidth = $(window).width();

    vid.parent().css('padding', 0);
    
    /* If the window is a 16:9 aspect ratio */
    if(windowWidth/windowHeight > aspect){
        var videoWidth = windowHeight * aspect;
        vid.css({'height': windowHeight*0.7, 'width' : videoWidth});
    /* If the window is less than a 16:9 aspect ratio */
    }else{
        var videoWidth = windowWidth;
        var videoHeight = windowHeight - 200; /* Make video the height of the window minus the vertical padding */
        vid.css({'height': videoHeight, 'width' : videoWidth});
    }
}