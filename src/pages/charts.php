<section id="charts-topbar" class="blue">
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-lg-12 wow fadeIn">
				<h1>Nightlife Charts</h1>
				<h3>Now you can listen to the music being played inside Australia's best venues!</h3>
            </div>
        </div>
    </div>
</section>
<section>
	<div class="container">
<p>Thanks to Nightlife's online reporting capabilities, we are able to share with you live song plays from Nightlife systems around the country.</p>
<p>Each week, a new playlist from a featured Nightlife client will be accessible for you to enjoy. </p>

<p>Clients can register their interest by calling 1800 773 468. Sharing the music being played from their big nights on their websites and with their Facebook subscribers s proving to be an easy way to build loyalty and drive patrons to  their venues. </p>

<p>In the meantime, sit back and enjoy some top tunes!</p>


	<div class="col-xs-12 col-sm-6 col-md-3">
		<h3>General / Bar</h3>
        <p>Listen & Follow</p>
     	<a class="playlog-launch" href="http://spoti.fi/1jJ9tFm" target="_blank" >
     		Spotify
		</a>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-3">
		<h3>Lounge Bar</h3>
        <p>Listen & Follow</p>
     	<a class="playlog-launch" href="http://spoti.fi/1gxSGkP" target="_blank" >
     		On Spotify <i class="fa fa-play"></i> 
		</a>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-3">
		<h3>Jukebox</h3>
        <p style="margin:0">Listen & Follow</p>
     	<a class="playlog-launch" href="http://spoti.fi/RkxnOf" target="_blank" target="_blank" >
     		On Spotify <i class="fa fa-play"></i> 
		</a>

      <p>Top 50 Chart:</p>
     	<a class="playlog-launch" href="http://spoti.fi/1mTl0FZ" target="_blank" >
     		On Spotify <i class="fa fa-play"></i> 
		</a>     	    
	</div>

	 <div class="col-xs-12 col-sm-6 col-md-3">
		<h3>DJ Top Plays</h3>
        <p style="margin:0">Listen & Follow</p>
     	<a class="playlog-launch" href="http://spoti.fi/1hDHirW" target="_blank" >
     		On Spotify <i class="fa fa-play"></i> 
		</a>
       <p>Top 50 Chart:</p>
     	<a class="playlog-launch" href="http://spoti.fi/1gxGjFQ" target="_blank" >
     		On Spotify <i class="fa fa-play"></i> 
		</a>  	
		</div>


	</div>
</section>