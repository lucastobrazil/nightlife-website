var os = require('os');
//var weinreAddr = '';
/*if(os.hostname() !== 'HDMSVPN') {
  weinreAddr = '<script src="http://dev-hdmsvpn.nightlife.com.au:9999/target/target-script-min.js#anonymous"></script>'; 
}*/

module.exports = function(grunt) {
  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
		  paths: ["less"],
          optimization: 2
        },
        files: {
          // target.css file: source.less file
          "src/css/vitality.css": "src/less/vitality.less"
        }
      }
    },
    watch: {
      styles: {
        files: ['src/less/**/*.less', "gruntfile.js"], // which files to watch
        tasks: ['less'],
        options: {
		  force: true,
          nospawn: true
        }
      }
    } 
  }); 

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', 'runs my tasks', function(){
	var tasks = ['watch', 'less'];
	//always use force when watching
	grunt.option('force', true);
	grunt.task.run(tasks);
  });
};