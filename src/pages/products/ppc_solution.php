<section id="mmn" class="row text-center" style="background-image: url('img/what_we_do/products/nightlife_karaoke.jpg'); color: white;">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-2">
		<div class="col-lg-8 col-lg-offset-2">
		    <i class="fa fa-microphone fa-4x"></i>
		    <h2>PPC Solution for Restaurants</h2>
		    <p>We will redefine your expectations of restaurant music.</p>
		    <hr class="colored">
		</div>
		<div class="col-lg-8 col-lg-offset-2">
		    <p>Our Public Performance Compliant solution for restaurants delivers cost effective background music with the strength of a foreground solution. We review every track and re-produce it so there are no awkward silences or strange noises. Just great music to complete your customers’ experience. </p>
		    <ul style="text-align: left;">
		        <li>Original music by well-known artists</li>
		        <li>Remastered with DJ style cue points</li>
		        <li>Cost effective</li>
		    </ul>
		</div>
	</div>
</section>