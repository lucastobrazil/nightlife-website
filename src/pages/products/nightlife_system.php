<section id="mmn" class="row text-center" style="background-image: url('img/what_we_do/products/nightlife_system.jpg'); color: white;">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-2">
		<div class="col-lg-8 col-lg-offset-2">
		    <i class="fa fa-microphone fa-4x"></i>
		    <h2>The Nightlife System</h2>
		    <hr class="colored">
		</div>
		<div class="col-lg-8 col-lg-offset-2">
		    <p>Made in and for Australian conditions – no it’s not a 4WD truck but it’s tough like one! Our flagship hardware gives you multi-zone audio and visuals, access to all of Nightlife Music’s great features including online support and app integration. Plus direct system control via an interface monitor.</p>
		    <p>There’s no need to worry about what hardware you need, as part of your solution design we’ll work out exactly what hardware will work best for you. </p>
		</div>
	</div>
</section>