                    <div class="panel">
                        <div class="panel-body">
                            <h3><i class="fa fa-envelope"></i> Quick Enquiry</h3>
                            <form name="sentMessage" id="contactForm" novalidate>
                                <div class="row control-group">
                                    <div class="form-group col-xs-12 floating-label-form-group controls">
                                        <label>Your Name</label>
                                        <input type="text" class="form-control" placeholder="Your Name" id="name" required data-validation-required-message="Please enter your name.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="row control-group">
                                    <div class="form-group col-xs-12 floating-label-form-group controls">
                                        <label>Email</label>
                                        <input type="email" class="form-control" placeholder="Email" id="email" required data-validation-required-message="Email address is required.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="row control-group">
                                    <div class="form-group col-xs-12 floating-label-form-group controls">
                                        <label>Contact Phone</label>
                                        <input type="tel" class="form-control" placeholder="Contact Phone" id="phone" required data-validation-required-message="Phone number is required.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 control-group">
                                        <div class="form-group floating-label-form-group controls">
                                            <label>Business Name</label>
                                            <input type="text" class="form-control" placeholder="Business Name" id="busname" required data-validation-required-message="Business name is required.">
                                            <p class="help-block text-danger"></p>
                                        </div>
                                    </div>  
                                    <div class="col-xs-12 col-sm-4 control-group">
                                        <div class="form-group floating-label-form-group controls">
                                            <label>Suburb</label>
                                            <input type="text" class="form-control" placeholder="Suburb" id="suburb" required data-validation-required-message="Suburb is required.">
                                            <p class="help-block text-danger"></p>
                                        </div>
                                    </div>                                                               
                                    <div class="col-xs-12 col-sm-4 control-group">
                                        <div class="form-group col-xs-12 floating-label-form-group controls">
                                            <label>Business Post Code</label>
                                            <input type="text" class="form-control" placeholder="Post Code" id="postcode" required data-validation-required-message="Post code is required.">
                                            <p class="help-block text-danger"></p>
                                        </div>
                                    </div>                                      
                                    <div class="col-xs-12 col-sm-4 control-group">
                                        <div class="form-group col-xs-12 floating-label-form-group controls">
                                            <label style="top: 0; /*hack */">State</label>
                                                <select id="state" name="state" class="form-control">
                                                  <option value="ACT">ACT</option>
                                                  <option value="NSW">NSW</option>
                                                  <option value="NT">NT</option>
                                                  <option value="QLD">QLD</option>
                                                  <option value="SA">SA</option>
                                                  <option value="TAS">TAS</option>
                                                  <option value="VIC">VIC</option>
                                                  <option value="WA">WA</option>
                                                </select>
                                            <p class="help-block text-danger"></p>
                                        </div>
                                    </div> 
                                </div>                                                                
                                <div class="row control-group">
                                    <div class="form-group col-xs-12 floating-label-form-group controls">
                                        <label>Message</label>
                                        <textarea rows="5" class="form-control" placeholder="Message" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <br>
                                <div id="success"></div>
                                <div class="row">
                                    <div class="form-group col-xs-12">
                                        <button type="submit" class="btn btn-outline-dark">Send</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>