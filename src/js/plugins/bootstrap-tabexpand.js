/* 
    TabExpand.js 
    Author: Lucas Arundell
    This is an amended version of the Bootstrap Tabcollapse plugin (https://github.com/flatlogic/bootstrap-tabcollapse). 
    Instead of creating an accordion of collapsed tabs, this plugin replaces the tabbed content with a vertical list of 
    Bootstrap Panels. These aren't collapsible; all they do is render out on the page vertically (on page-xs)

*/
!function ($) {

    "use strict";

    // TABEXPAND CLASS DEFINITION
    // ======================

    var TabExpand = function (el, options) {
        this.options   = options;
        this.$tabs  = $(el);

        this._panelsVisible = false; //content is attached to tabs at first
        this._initPanels();
        this._checkStateOnResize();


        // checkState() has gone to setTimeout for making it possible to attach listeners to
        // shown-accordion.bs.tabexpand event on page load.
        // See https://github.com/flatlogic/bootstrap.tabexpand/issues/23
        var that = this;
        setTimeout(function() {
          that.checkState();
        }, 0);
    };

    TabExpand.DEFAULTS = {
        panelClass: 'visible-xs',
        tabsClass: 'hidden-xs',
        panelTemplate: function(heading, groupId, parentId, active) {
            return  '<div class="panel panel-default">' +
                    '   <div class="panel-heading">' +
                    '      <h4 class="panel-title">' +
                    '      </h4>' +
                    '   </div>' +
                    '   <div id="' + groupId + '" class=" ' + (active ? 'in' : '') + '">' +
                    '       <div class="panel-body js.tabexpand-panel-body">' +
                    '       </div>' +
                    '   </div>' +
                    '</div>'
        }
    };

    TabExpand.prototype.checkState = function(){
        if (this.$tabs.is(':visible') && this._panelsVisible){
            this.showTabs();
            this._panelsVisible = false;
        } else if (this.$tabExpandPanel.is(':visible') && !this._panelsVisible){
            this.showAccordion();
            this._panelsVisible = true;
        }
    };

    TabExpand.prototype.showTabs = function(){
        var view = this;
        this.$tabs.trigger($.Event('show-tabs.bs.tabexpand'));

        var $panelHeadings = this.$tabExpandPanel.find('.js.tabexpand-panel-heading').detach();
        $panelHeadings.each(function() {
            var $panelHeading = $(this),
                $parentLi = $panelHeading.data('bs.tabexpand.parentLi');
            view._panelHeadingToTabHeading($panelHeading);
            $parentLi.append($panelHeading);
        });

        var $panelBodies = this.$tabExpandPanel.find('.js.tabexpand-panel-body');
        $panelBodies.each(function(){
            var $panelBody = $(this),
                $tabPane = $panelBody.data('bs.tabexpand.tabpane');
            $tabPane.append($panelBody.children('*').detach());
        });
        this.$tabExpandPanel.html('');

        this.$tabs.trigger($.Event('shown-tabs.bs.tabexpand'));
    };

    TabExpand.prototype.showAccordion = function(){
        this.$tabs.trigger($.Event('show-accordion.bs.tabexpand'));

        var $headings = this.$tabs.find('li:not(.dropdown) [data-toggle="tab"], li:not(.dropdown) [data-toggle="pill"]'),
            view = this;
        $headings.each(function(){
            var $heading = $(this),
                $parentLi = $heading.parent();
            $heading.data('bs.tabexpand.parentLi', $parentLi);
            view.$tabExpandPanel.append(view._createAccordionGroup(view.$tabExpandPanel.attr('id'), $heading.detach()));
        });

        this.$tabs.trigger($.Event('shown-accordion.bs.tabexpand'));
    };

    TabExpand.prototype._panelHeadingToTabHeading = function($heading) {
        var href = $heading.attr('href').replace(/-collapse$/g, '');
        $heading.attr({
            'data-toggle': 'tab',
            'href': href,
            'data-parent': ''
        });
        return $heading;
    };

    TabExpand.prototype._tabHeadingToPanelHeading = function($heading, groupId, parentId, active) {
        $heading.addClass('js.tabexpand-panel-heading ');
        $heading.attr({
            'data-toggle': '',
            'data-parent': '#' + parentId,
            'href': '#' + groupId
        });
        return $heading;
    };

    TabExpand.prototype._checkStateOnResize = function(){
        var view = this;
        $(window).resize(function(){
            clearTimeout(view._resizeTimeout);
            view._resizeTimeout = setTimeout(function(){
                view.checkState();
            }, 100);
        });
    };


    TabExpand.prototype._initPanels = function(){
        this.$tabExpandPanel = $('<div class="nl-tab-panel panel-group ' + this.options.panelClass + '" id="' + this.$tabs.attr('id') + '-panel' +'"></div>');
        this.$tabs.after(this.$tabExpandPanel);
        this.$tabs.addClass(this.options.tabsClass);
        this.$tabs.siblings('.tab-content').addClass(this.options.tabsClass);
    };

    TabExpand.prototype._createAccordionGroup = function(parentId, $heading){
        var tabSelector = $heading.attr('data-target'),
            active = $heading.data('bs.tabexpand.parentLi').is('.active');

        if (!tabSelector) {
            tabSelector = $heading.attr('href');
            tabSelector = tabSelector && tabSelector.replace(/.*(?=#[^\s]*$)/, ''); //strip for ie7
        }

        var $tabPane = $(tabSelector),
            groupId = $tabPane.attr('id') + '-collapse',
            $panel = $(this.options.panelTemplate($heading, groupId, parentId, active));
        $panel.find('.panel-heading > .panel-title').append(this._tabHeadingToPanelHeading($heading, groupId, parentId, active));
        $panel.find('.panel-body').append($tabPane.children('*').detach())
            .data('bs.tabexpand.tabpane', $tabPane);

        return $panel;
    };



    // TABCOLLAPSE PLUGIN DEFINITION
    // =======================

    $.fn.tabExpand = function (option) {

    /*
    1. Find all Tabs and change to h1
    2, Find all containers and change to span

    */
        // var tabs = [];
        // $('.collabsible-tabs').find('li a').each(function( index ) {
        //    tabs.push($( this ).text() );
        // });
        // console.log(tabs);

        // var tabcontent = [];
        // $('.tab-content').find('.tab-pane').each(function( index ) {
        //    tabcontent.push($( this ).html() );
        // });
        // console.log(tabcontent);

        // $('.home-tabpanel').html('');
        // $.each(tabs, function(index, value){
        //     console.log('tab = ' + index); 
        //     $('.home-tabpanel').prepend('<h1>' + value + '</h1><div>' + tabcontent[index] + '</div>');
        // });

        return this.each(function () {
            var $this   = $(this);
            var data    = $this.data('bs.tabexpand');
            var options = $.extend({}, TabExpand.DEFAULTS, $this.data(), typeof option === 'object' && option);

            if (!data) $this.data('bs.tabexpand', new TabExpand(this, options));
        });

        
    };

    $.fn.tabExpand.Constructor = TabExpand;


}(window.jQuery);
