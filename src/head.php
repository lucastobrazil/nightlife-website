<?php
    include('php/functions.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    
    <!-- Metadata -->
    <!-- A lot of these php variables are stored in php/functions.php -->
    <title><?php echo $pageTitle; ?></title>
    <meta name="description" content="<?php echo $pageMetaDescription; ?>">
    <meta name="author" content="<?php echo $companyName;?>">

    <!-- Facebook OpenGraph Content -->
    <meta property="og:title" content="<?php echo $pageTitleText . " - " . $pageTitleExtra;?>">
    <meta property="og:site_name" content="<?php echo $companyName;?>">
    <meta property="og:url" content="<?php echo $pageURL;?>">
    <meta property="og:description" content="<?php echo $pageMetaDescription;?>">
    
    <link rel="shortcut icon" type="image/png" href="img/favicon.png">
    <link rel="icon" type="image/png" href="img/favicon.png"

    <!-- Nightstrap Core CSS -->
    <link href="css/lib/nightstrap/nightstrap.css" rel="stylesheet" type="text/css">
    
    <!-- Retina.js - Load first for faster HQ mobile images. -->
    <script src="js/plugins/retina/retina.min.js"></script>

    <!-- Plugin CSS -->
    <link href="css/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="css/plugins/owl-carousel/owl.theme.css" rel="stylesheet" type="text/css">
    <link href="css/plugins/owl-carousel/owl.transitions.css" rel="stylesheet" type="text/css">
    <link href="css/plugins/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="css/plugins/background.css" rel="stylesheet" type="text/css">
    <link href="css/plugins/animate.css" rel="stylesheet" type="text/css">
    
    <!-- Vitality Theme CSS -->
    <link href="css/vitality.css" rel="stylesheet" type="text/css">
    <!-- IE8 support for HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Swiftype Search bar plugin code. -->
    <!-- Go to http://swiftype.com/engines/nightlife-website-search/overview -->
    <script type="text/javascript">
      (function(w,d,t,u,n,s,e){w['SwiftypeObject']=n;w[n]=w[n]||function(){
      (w[n].q=w[n].q||[]).push(arguments);};s=d.createElement(t);
      e=d.getElementsByTagName(t)[0];s.async=1;s.src=u;e.parentNode.insertBefore(s,e);
      })(window,document,'script','//s.swiftypecdn.com/install/v2/st.js','_st');

      _st('install','M_Zam3fErFiSa5dyzQ5M','2.0.0');
    </script>


    <!-- Google analytics tracking code -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-10974033-1', 'auto');
      ga('send', 'pageview');

    </script>
</head>

<!-- Body Begin -->
<body id="page-top" style="position: relative;">