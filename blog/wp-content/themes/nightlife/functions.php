<?php 


	// Allow the Theme to use custom Menus
	register_nav_menus( array(
		'top-navigation' => __( 'Top Navigation')
	) );



function NLgenerate_menu()
{
	$menu_name = 'top-navigation';
	$locations = get_nav_menu_locations();
	$menu = wp_get_nav_menu_object($menu_name);
	$menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );

	/*var_dump($menuitems);*/
	$output = '<nav><ul class="nav navbar-nav navbar-right">';

    $count = 0;
    $submenu = false;

    foreach( $menuitems as $item ){
        // get page id from using menu item object id
        $id = get_post_meta( $item->ID, '_menu_item_object_id', true );
        // set up a page object to retrieve page data
        $page = get_page( $id );
        $link = get_page_link( $id );

        // item does not have a parent so menu_item_parent equals 0 (false)
        if ( !$item->menu_item_parent ){

	        // save this id for later comparison with sub-menu items
	        $parent_id = $item->ID;
	 
		    $output .= '<li class="item"><a href="'. $link . '" class="title">';
		    $output .= $page->post_title;
		    $output .= '</a>';
			$output .='<a href="' . $link . '" class="desc">';
		    $output .=$page->post_excerpt;
		    $output .= '</a>';
    	};
 
     	if ( $parent_id == $item->menu_item_parent ){
 
            if ( !$submenu ){
            	$submenu = true;
            	$output .='<ul class="sub-menu">';
            };
 
            $output .='<li class="item">';
            $output .='<a href="'. $link . '" class="title">' . $page->post_title . '</a>';
            $output .='<a href="'. $link . '" class="desc">'. $page->post_excerpt . '</a>';
            $output .='</li>';
 
            if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ){
	            $output .='</ul>';
	            $submenu = false; 
            };
 
        };
 
	    if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ){
		    $output .='</li>';
		    $submenu = false; 
	   	};
	 
		$count++; 
	};
 
	$output .='</ul>';
	$output .='</nav>';

	echo $output;
}

function load_scripts()
{

	// Core Scripts -->
	wp_register_script( 'jquery', get_template_directory_uri() . '/js/plugins/jquery.js');
	wp_enqueue_script( 'jquery' );

	wp_register_script( 'bootstrap-min', get_template_directory_uri() . '/js/bootstrap/bootstrap.min.js');
	wp_enqueue_script( 'bootstrap-min' );
	wp_register_script( 'test', get_template_directory_uri() . '/js/test.js');
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'test' );

	// Retina.js - Load first for faster HQ mobile images.
	// Register the script like this for a theme:
	wp_register_script( 'retina-js', get_template_directory_uri() . '/js/plugins/retina/retina.min.js');
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'retina-js' );


	
	wp_register_script( 'jquery-easing', get_template_directory_uri() . '/js/plugins/jquery.easing.min.js');
	wp_enqueue_script( 'jquery-easing' );	
    
    // Plugin Scripts -->	
	wp_register_script( 'classie', get_template_directory_uri() . '/js/plugins/classie.js');
	wp_enqueue_script( 'classie' );

	wp_register_script( 'cbpAnimatedHeader', get_template_directory_uri() . '/js/plugins/cbpAnimatedHeader.js');
	wp_enqueue_script( 'cbpAnimatedHeader' );
	
	wp_register_script( 'owl-carousel', get_template_directory_uri() . '/js/plugins/owl-carousel/owl.carousel.js');
	wp_enqueue_script( 'owl-carousel' );

	wp_register_script( 'jquery-magnific-popup', get_template_directory_uri() . '/js/plugins/jquery.magnific-popup/jquery.magnific-popup.min.js');
	wp_enqueue_script( 'jquery-magnific-popup' );
		
	wp_register_script( 'jquery-fs-wallpaper', get_template_directory_uri() . '/js/plugins/jquery.fs.wallpaper.js');
	wp_enqueue_script( 'jquery-fs-wallpaper' );
		
	wp_register_script( 'jquery.mixitup', get_template_directory_uri() . '/js/plugins/jquery.mixitup.js');
	wp_enqueue_script( 'jquery.mixitup' );
	
	wp_register_script( 'wow', get_template_directory_uri() . '/js/plugins/wow/wow.min.js');
	wp_enqueue_script( 'wow' );
	
	wp_register_script( 'contact_me', get_template_directory_uri() . '/js/contact_me.js');
	//wp_enqueue_script( 'contact_me' );
		
	wp_register_script( 'jqBootstrapValidation', get_template_directory_uri() . '/js/plugins/jqBootstrapValidation.js');
	wp_enqueue_script( 'jqBootstrapValidation' );


	
	// Vitality Theme Scripts -->
	wp_register_script( 'vitality', get_template_directory_uri() . '/js/vitality.js');
	wp_enqueue_script( 'vitality' );

}
add_action( 'wp_footer', 'load_scripts','','',true);


?>