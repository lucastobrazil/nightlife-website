<section id="mmn" class="row">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-2">
		<div class="col-sm-12  text-center">
		    <img src="img/icons/manage_my_nightlife_icon.jpg" class="img-responsive img-centered" alt="">
		    <h2>Manage My Nightlife</h2>
		    <hr class="colored">
		    <h3>Whether you are on your smartphone, tablet or desktop you have total control with Manage My Nightlife.</h3>
		</div>
		<div class="col-md-8">
		    <p><i class="fa fa-mobile"></i> On your mobile devices you can search, load and preview music, adjust the volume, control your visuals plus run bingo, draws and karaoke. </p>
		    <p><i class="nm-icons-advertising"></i> Access it via your desktop and Manage My Nightlife also becomes your portal to load, manage and organise your advertising. Whether you are in your office, on the floor or at the airport lounge Manage My Nightlife gives you control over what’s happening in your venue at the touch of a button. </p>
		    <div class="col-xs-12 col-sm-6">
		        <a href="https://play.google.com/store/apps/details?id=com.hdms.manager" target="_blank">
		            <img src="img/icons/android_app_on_google_play.png" class="img-responsive" />
		        </a>
		    </div>
		    <div class="col-xs-12 col-sm-6">
		        <a href="https://itunes.apple.com/au/app/manage-my-nightlife/id867370907?ls=1&mt=8" target="_blank">
		            <img src="img/icons/available_on_the_app_store.png" class="img-responsive"  />
		        </a>
		    </div>
		</div>
		<div class="col-md-4">
		    <img src="img/what_we_do/products/manage_my_nightlife_mbp.png" class="img-responsive img-centered" alt="">
		</div>
	</div>
</section>