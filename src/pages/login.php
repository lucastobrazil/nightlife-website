<section id="login">
<div class="container-fluid">
  <div class="row text-center wwd-grid" style="padding:20px;">
    <h1>Login</h1>
    <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
        <span>
            <h3>Manage My Nightlife <i class="nm-icons-mmn"></i></h3>
            <p>Control the music, visuals and configure the Nightlife system in your business.</p>
            <div class="row">
              <div class="col-xs-12 col-sm-4 col-sm-offset-2">
                  <a href="https://play.google.com/store/apps/details?id=com.hdms.manager" target="_blank">
                      <img src="img/icons/android_app_on_google_play.png" class="img-responsive" />
                  </a>
              </div>
              <div class="col-xs-12 col-sm-4">
                  <a href="https://itunes.apple.com/au/app/manage-my-nightlife/id867370907?ls=1&mt=8" target="_blank">
                      <img src="img/icons/available_on_the_app_store.png" class="img-responsive"  />
                  </a>
              </div>
            </div>
            <hr class="colored" />
            <a class="col-xs-12 col-sm-6 col-sm-offset-3" href="http://hdms-live.nightlife.com.au/manager/" target="_blank">
                <button class="btn btn-primary">Login to Web App <i class="fa fa-angle-right"></i></button>
            </a>
          
        </span>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <span>
            <h3>Nightlife Enterprise</h3>
            <p>Coming Soon!</p>
            <a href="<?php echo $what_we_do; ?>#products" class="page-scroll">
                <button class="btn btn-success">Find Out More <i class="fa fa-angle-right"></i></button>
            </a>
        </span>
    </div>
</div>
</section>