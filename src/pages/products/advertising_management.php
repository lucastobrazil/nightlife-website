<section id="mmn" class="row text-center"style="background-image: url('img/what_we_do/products/advertising_management.jpg'); color: white;">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-2">
        <div class="col-lg-8 col-lg-offset-2">
            <i class="fa fa-image fa-4x"></i>
            <h2>Advertising Management</h2>
            <hr class="colored">
        </div>
        <div class="col-lg-8 col-lg-offset-2">
            <p>With integrated digital advertising and create-your-own templates the Nightlife System offers a complete advertising and promotion system for your business.</p>
            <p>Whatever your messages you can share them with your customers and manage them easily with our range of tools. Manage My Nightlife lets you manage your screens from your smart phone and your advertising scheduling from your desktop.</p>
            <p>You can set display times, promotion periods and frequency settings to make sure that the messages you want display when you want them to.</p>

        </div>
	</div>
</section>