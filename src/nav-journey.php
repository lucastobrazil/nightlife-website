<nav class="navbar journey-nav invisible">
    <div class="container">
        <ul class="nav navbar-nav">           
            <li class="callout-bottom">
                <a href="#introvideo" class="page-scroll">Since the Beginning</a>
            </li>
            
            <li class="callout-bottom">
                <a href="#weredifferent" class="page-scroll">We're Different</a>
            </li >
           <!-- <li class="callout-bottom">
                <a href="#designing" class="page-scroll">Designing Your Solution</a>
            </li>-->
            <li class="callout-bottom">
                <a href="#weknow" class="page-scroll">We Know Your Space</a>
            </li>

        </ul>
    </div>
</nav>