<section id="mmn" class="row text-center" style="background-image: url('img/what_we_do/products/random_number_generator.jpg'); color: white;">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-2">
		<div class="col-lg-8 col-lg-offset-2">
            <i class="fa fa-gamepad fa-4x"></i>
            <h2>Bingo</h2>
            <h3>Snakes alive 55, camomile tea 73, dancing queen 17…Bingo! <h3>
            <hr class="colored">
        </div>
        <div class="col-lg-8 col-lg-offset-2">
            <p>That’s right, there is no better way to spice up a quiet night in your venue than with a great round of Bingo. </p>
            <p>Bingo is making a comeback and with Nightlife it is all integrated with your system, controllable from your smart phone with Manage My Nightlife and comes complete with traditional call prompts to keep the nostalgia alive. </p>
        </div>
	</div>
</section>