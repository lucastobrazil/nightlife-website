<section id="careers">
	<div class="container">
			<h1 class="col-xs-12">Careers</h1>
		<div id="careers" class="col-xs-12 col-md-8">
			<h3>Be Part of It</h3>
			<p>Innovation, ingenuity and team work. If that sounds like a good day at work to you then you’ll love Nightlife Music. Throw in free breakfasts, Ice-cream Mondays and an awesome soundtrack and you’ll quickly realise why so many of our team have been here for so long! </p>
			<p>We are always looking for talented people to join our team. Check out any current vacancies below or if you don’t spot anything for you but you think you’re the perfect fit then let us know! </p>

			<p>Send your resume and cover letter to <a href='mailt&#111;&#58;hr%&#52;&#48;&#110;&#105;ght&#108;&#105;&#102;%65&#46;com&#46;&#97;u'>hr&#64;&#110;i&#103;htlife&#46;com&#46;au</a>. Don't forget to include your department of interest in the subject line (e.g. Sales, Marketing, Accounts Administration, Client Services, Manufacturing, Warehouse, R&D, Music Production, IT or Software Development). </p>
			<p align="center">
				<a class="inline-play" data-cat="web" href="#videoModal" data-target="#videoModal" data-toggle="modal">
					<i class="fa fa-play"></i>
				</a>
			</p>

		</div>
		<div id="careers" class="col-xs-12 col-md-4">
			<h3>Positions Available</h3>
			<p>
				<!--<a href="img/careers/sample_job.jpg">
					<img src="img/careers/sample_job.jpg" class="img-responsive" />
				</a>-->
					<?php echo getDirectory( "datastore/pdf/positions_available" );  ?>
			</p>
		</div>

	</div>
</section>


<!-- Infographic Video Modal -->
<div class="portfolio-modal modal fade video-modal" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal"> 
            <button class="btn btn-default"><i class="fa fa-angle-left"></i> Back</button>
         </div>
        <div class="modal-body">
            <video id="infographic_video" class="video-js vjs-default-skin fs-video" controls data-setup='{"autoplay": true, "preload": "auto" }'height="200" >
                <source src="mp4/NL_Culture_20150630_720p25_Web-Ready.mp4" type="video/mp4" />
                <source src="mp4/NL_Culture_20150630_720p25_Web-Ready.webm" type="video/webm" />
                <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
            </video>
        </div>
    </div>
</div>