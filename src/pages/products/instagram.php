<section id="mmn" class="row text-center" style="background-image: url('img/what_we_do/products/instagram_bg.jpg'); color: white">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-2">
		<div class="col-lg-8 col-lg-offset-2">
            <img src="img/icons/instagram_nightlife.jpg" class="img-responsive img-centered" alt="Nightlife and Instagram">
            <h2>Instagram Integration</h2>
            <h3>#selfie #lovethisplace #yourbusiness</h3>
            <p>Want to see this kind of love on your screens?</p>
            <hr class="colored">
        </div>
        <div class="col-lg-8 col-lg-offset-2">
            <p> Now you can with our Instagram Feed feature. A picture tells a thousand words and Instagram is the perfect channel to share your businesses love with your customers. </p>
            <p>Display a user account or follow a hashtag. The images will scroll through on screen and update with any new images added to the feed.</p>
            <p>It’s a great way to connect with your customers, start a conversation and drive your social media followers to help you keep the conversation going after they’ve left. 
            </p>
        </div>
	</div>
</section>