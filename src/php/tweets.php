    <h2 class="col-sm-hidden col-md-2">
    	Latest <i class="fa fa-twitter"></i>
    </h2> 
    <div id="tweet-container" class="col-sm-12 col-md-9">
	    <span class='col-xs-1 slide-control prev'>&lsaquo;</span>
		<div id="tweet-slides" class="col-xs-10">
			<?php 
			include('tweet_get.php');
			if( is_array( $data ) && count( $data ) > 0 ) {
				foreach($data as $tweet)
				{
					$tweet_text = $tweet->text;
					$date = $tweet->created_at;
					$formatted_date = date('F jS', strtotime($date));

					$logo = $tweet->user->profile_image_url;
					$img = $tweet->entities->media[0]->media_url;
					//echo "image = " . $img;

					$pattern = '#(www\.|https?://)?[a-z0-9]+\.[a-z0-9]{2,4}\S*#i'; //RegEX for finding http links
				   
				    if (preg_match($pattern, $tweet_text, $matches)){
						$text = $matches[0];	                       
					}
					else
					{
				    	$text = "||";
				    }

				    $linkedText = "<a href='{$text}'target='_blank' class='green'>Read More</a>";

				    $tweet_text = str_replace($text, $linkedText, $tweet_text);

					echo "<span>";
					echo "<span class='social-content'>";
					if($img){
						echo "<span class='social-badge' style='background-image:url({$img});'></span>";
					}else{
						echo "<span class='social-badge' style='background-image:url({$logo});'></span>";
					}
					echo "<span class='social-date'>{$formatted_date}</span>{$tweet_text}</span>";
					echo "</span>";
				}
			}
			?>
	    </div>
    	<span class='col-xs-1 slide-control next'>&rsaquo;</span>
    </div>
