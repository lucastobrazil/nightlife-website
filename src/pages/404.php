<aside class="cta-quote" style="background-image: url('img/bg-sky.jpg');">
    <div class="container wow fadeIn">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
            	<h1><img src="img/nightlife_music_microphone.svg" class="animated pulse infinite" /></h1>
                <h3>404 Error</h3> 
                <span class="quote">Ground control to <span class="text-primary">Major Tom!</span> It seems the page you're looking for <span class="text-primary">can't be found</span>.</span>
                <hr class=" colored">
                <a class="btn btn-outline-light page-scroll" href="<?php echo $home; ?>">Turn your ship around <i class="fa fa-home"></i></a>
            </div>
        </div>
    </div>
</aside>