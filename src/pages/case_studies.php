<section id="case_studies">
	<div class="container text-center">
		<h1>Case Studies</h1>
	</div>
</section>

    <section class="clients-carousel standard-carousel wow fadeIn">
        <div class="item matisse">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="project-details">
                            <span class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <span class="col-xs-12">
                                    <span class="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                        <img src="img/client-logos/matisse-logo-gradient.svg" class="client-logo img-responsive" alt="">
                                    </span>
                                </span>
                                <span class="project-description quote">"Thanks to Nightlife Music we have a custom curated playlist that adds the right amount of glamour and relaxation across all areas of the venue."</span>
                                <span class="project-description author">Gina Brand, Venue Manager - <a href="http://www.matissebeachclub.com.au" target="_blank">Matisse Beach Club <i class="fa fa-external-link"></i></a></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item snapfitness">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="project-details">
                            <span class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <span class="col-xs-12">
                                    <span class="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                        <img src="img/client-logos/logo_snap_fitness.png" class="client-logo img-responsive" alt="">
                                    </span>
                                </span>
                                <span class="project-description quote">"Our members love the music, especially choosing songs with the kiosk! We see fewer headphones, enjoy more interaction and we still have total control!"</span>
                                <span class="project-description author">David Noonan, Managing Director - <a href="http://www.snapfitness.com.au/" target="_blank">Snap Fitness <i class="fa fa-external-link"></i></a></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item frankstonrsl">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="project-details">
                            <span class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <span class="col-xs-12">
                                    <span class="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
                                        <img src="img/client-logos/logo_frankston_rsl.png" class="client-logo img-responsive" alt="">
                                    </span>
                                </span>
                                <span class="project-description quote">"Nightlife has made our lives easy. We manage our music, raffles, promotions and visuals from one spot and we use their mobile apps to do it all while on the go. "</span>
                                <span class="project-description author"><a href="http://www.frankstonrsl.com.au/" target="_blank">Frankston RSL <i class="fa fa-external-link"></i></a></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        <div class="item collaroyhotel">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="project-details">
                            <span class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <span class="col-xs-12">
                                    <span class="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
                                        <img src="img/client-logos/logo_collaroy_hotel.png" class="client-logo img-responsive" alt="">
                                    </span>
                                </span>
                                <span class="project-description quote">"Music was critical to getting the positioning right with the Collaroy. Nightlife helped make sure that the experience is something that locals could relate to, but also a signal that they were somewhere pretty special."</span>
                                <span class="project-description author">Alistair Flower, General Manager - <a href="http://www.collaroyhotel.com.au/" target="_blank">Collaroy Hotel <i class="fa fa-external-link"></i></a></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item vivalavida">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="project-details">
                            <span class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                                <span class="col-xs-12">
                                    <span class="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                                        <img src="img/client-logos/logo_viva_la_vida.png" class="client-logo img-responsive" alt="">
                                    </span>
                                </span>
                                <span class="project-description quote">"We needed someone who could find us the best music and keep it constantly fresh. Nightlife are the experts and they are always there to help us out!"</span>
                                <span class="project-description author">Kate Staples, Director - <a href="http://vivalavidadarwin.com.au/" target="_blank"> Viva La Vida Wine & Tapas Bar <i class="fa fa-external-link"></i></a></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </section>