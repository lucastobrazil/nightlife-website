<section id="mmn" class="row text-center"  style="background-image: url('img/what_we_do/products/nightlife_scheduler.jpg'); color: white;">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-2">
		<div class="col-lg-8 col-lg-offset-2">
	        <i class="fa fa-calendar fa-4x"></i>
	        <h2>Scheduler</h2>
	        <h3>Your music matters most when you’re busy and when you’re busy you just need it to work. </h3>
	        <hr class="colored">
	    </div>
	    <div class="col-lg-8 col-lg-offset-2">
	        <p>Our powerful scheduler makes it easy to always have the right music playing automatically.</p>
	        <p>Need to soothe a morning crowd and energise your way through the lunchtime rush? We can schedule that.</p>
	        <p>Do your midday clients prefer jazz but your evening customers want Top 40? We can schedule that.</p>
	        <p>Do your customers change every hour and you want your music to change as well? We can schedule that.</p>
	        <p>From open through to close every day, we schedule your system down to the minute with hand-picked music and visuals to match your unique trading patterns.</p>
	    </div>
	</div>
</section>