<?php

// =============================================================================
// FUNCTIONS.PHP
// -----------------------------------------------------------------------------
// Overwrite or add your own custom functions to X in this file.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Enqueue Parent Stylesheet
//   02. Additional Functions
// =============================================================================

// Enqueue Parent Stylesheet
// =============================================================================

add_filter( 'x_enqueue_parent_stylesheet', '__return_true' );



// Additional Functions
// =============================================================================


// Breadcrumb Home Text
// =============================================================================

//$main_site_URL_base = "http://lucas.new-devintranet.nightlife.com.au/design_tests/experiments/nightlife-website";
$main_site_URL_base = "/";
//$main_site_URL_base = "http://localhost/nightlife-website";

if ( ! function_exists( 'x_get_breadcrumb_home_text' ) ) :
  function x_get_breadcrumb_home_text() {
	return '<span class="home"><i class="x-icon-home"></i> Blog</span>';
  }
endif;
