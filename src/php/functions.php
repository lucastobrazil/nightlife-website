<?php 

//Routing of Pages 
$page = isset($_GET['page']) ? trim(strtolower($_GET['page'])) : "home";
$isDev = false;

// Handle Some case issues and spelling errors
if($page === 'crowdDJ' || $page ==='crowdDj' || $page === 'CrowdDJ'){   $page = 'crowddj';     }
if($page === 'ManageMyNightlife' || $page === 'managemynightlife'){   $page = 'mmn';     }
if($page === 'Enterprise'){   $page = 'enterprise';     }
if($page === 'Scheduler'){   $page = 'scheduler';     }
if($page === 'Instagram'){   $page = 'instagram';     }
if($page === 'Karaoke'){   $page = 'karaoke';     }
if($page ==='Nightlife-System' || $page === 'Nightlife-system' || $page === 'nightlife-System' || $page === 'nightlifesystem'){   $page = 'nightlife-system';     }
if($page === 'Nightlife-Microsystem' || $page === 'Nightlife-microsystem'|| $page === 'nightlife-Microsystem'|| $page === 'nightlifemicrosystem'|| $page === 'NightlifeMicrosystem'){   $page = 'nightlife-microsystem';     }
if($page === 'PPC' || $page === 'ppc' || $page === 'PPC-solution' || $page === 'ppc-Solution' || $page === 'ppcsolution'){   $page = 'ppc-solution';     }
if($page === 'Jukebox' || $page === 'juke'){   $page = 'jukebox';     }
if($page === 'touchpanel' || $page === 'TouchPanel' || $page === 'Touchpanel'|| $page === 'Touch-panel' || $page === 'touch-Panel' || $page === 'touch-Panel'){   $page = 'touch-panel';     }
if($page === 'rng' || $page === 'RandomNumberGenerator' || $page === 'randomnumbergenerator' || $page === 'Random-number-generator'){   $page = 'random-number-generator';     }
if($page === 'Bingo'){   $page = 'bingo';     }
if($page === 'scrollingtext' || $page === 'ScrollingText' || $page === 'Scrollingtext' || $page === 'Scrolling-Text'){   $page = 'scrolling-text';     }
if($page === 'picturegallery'|| $page === 'PictureGallery' || $page === 'Picture-gallery' || $page === 'picture-Gallery' || $page === 'Picture-Gallery'){   $page = 'picture-gallery';     }
if($page === 'advertisingmanagement' || $page === 'AdvertisingManagement' || $page === 'Advertising-Management' || $page === 'advertising-Management' || $page === 'Advertising-management'){   $page = 'advertising-management';     }


$allowedPages = array(
    'home'          => 'pages/home.php',
    'what-we-do'    => 'pages/what_we_do.php',
    'faq'           => 'pages/faq.php',
    'case-studies'  => 'pages/case_studies.php',
    'our-story'     => 'pages/our_story.php',
    'careers'       => 'pages/careers.php',
    'latest'        => 'pages/latest.php',
    'login'         => 'pages/login.php',
    'contact'       => 'pages/contact.php',
    'page404'       => 'pages/404.php',
    'charts'        => 'pages/charts.php',
    'crowddj'       => 'pages/products/crowddj.php',
    'mmn'           => 'pages/products/mmn.php',
    'enterprise'    => 'pages/products/enterprise.php',
    'scheduler'    => 'pages/products/scheduler.php',
    'instagram'    => 'pages/products/instagram.php',
    'karaoke'    => 'pages/products/karaoke.php',
    'nightlife-system'    => 'pages/products/nightlife_system.php',
    'nightlife-microsystem'    => 'pages/products/nightlife_microsystem.php',
    'ppc-solution'    => 'pages/products/ppc_solution.php',
    'jukebox'    => 'pages/products/jukebox.php',
    'touch-panel'    => 'pages/products/touch_panel.php',
    'random-number-generator'    => 'pages/products/random_number_generator.php',
    'bingo'    => 'pages/products/bingo.php',
    'scrolling-text'    => 'pages/products/scrolling_text.php',
    'picture-gallery'    => 'pages/products/picture_gallery.php',
    'advertising-management'    => 'pages/products/advertising_management.php'
);

// set up some link variables so it's easier to echo inline
// this way, we say <a href=<?php echo $our_story; > etc.
// if($isDev){    $basePage       = '/design_tests/nightlife-website/src/'; }
// Lucas Macbook Path
if($isDev){    $basePage       = '/nightlife/nightlife-website/src/'; }
else{    $basePage       = '/';    }

$home           = $basePage . '';
$what_we_do     = $basePage . 'what-we-do';
$faq            = $basePage . 'faq';
$case_studies   = $basePage .'case-studies';
$our_story      = $basePage .'our-story';
$careers        = $basePage .'careers';
$latest         = $basePage . 'latest';
$login          = $basePage . 'login';
$contact        = $basePage . 'contact';
$charts         = $basePage . 'charts';
$page404        = $basePage . '404';

// For Facebook OpenGraph
$pageURL = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

function setPageDetails($p)
{
    global $pageTitle, $pageMetaDescription, $pageTitleText, $pageTitleExtra, $companyName;
    $companyName = "Nightlife Music";

    // These parameters set the <title>, <meta description> and facebook open graph <og:> tags
    
    /*
        $pageTitleText - the page's basic title (eg About Us)
        $pageTitleExtra - an extended description (eg Leading Providers of .....)

        The <title> tags will be populated with the two above variables combined into $pageTitle. 
        It's important to still keep it short. 

        You can tweak each page's formatting of $companyName, $pageTitleText and $pageTitleExtra in the 
        switch statement below.

    */

    switch($p)
    {
        case "home":
            $pageTitleText = "Harness The Power of Music";
            $pageTitle = $companyName . " - " . $pageTitleText;
            $pageMetaDescription = "Nightlife Music is Australia’s premium background music and vision provider. Whatever your business we can help create the perfect soundtrack, just for you.";
            break;
        case "what-we-do":
            $pageTitleText = "What We Do";
            $pageTitleExtra = "Music, Visuals, Advertising, Licensing, Apps";
            $pageTitle = $pageTitleText . " - " . $companyName . " - " . $pageTitleExtra;
            $pageMetaDescription = "Nightlife Music provides a complete music, visual and advertising solution for your business. We can help you translate your brand into sound and vision.";
            break;
        case "case-studies":
            $pageTitleText = "Case Studies";
            $pageTitleExtra = " Over 2800 Venues Services Australia-Wide";
            $pageTitle = $pageTitleText . " - " . $companyName . " - " . $pageTitleExtra;
            $pageMetaDescription = "We create unique solutions for every one of our clients. Learn about our how we helped these brands complete their customer experience.";
            break;
        case "our-story":
            $pageTitleText = "Our Story";
            $pageTitleExtra = "Since 1989";
            $pageTitle = $pageTitleText . " - " . $companyName . " - " . $pageTitleExtra;
            $pageMetaDescription = "Nightlife Music began over 25 years ago with two mates and an idea that every venue should be able to easily play music that is chosen just for them.";
            break;
        case "faq":
            $pageTitleText = "Frequently Asked Questions";
            $pageTitle = $pageTitleText . " - " . $companyName . " - " . $pageTitleExtra;
            $pageMetaDescription = "Want to know more? Call us anytime on 1800 679 748 or check out our Frequently Asked Questions (FAQs) for more information.";
            break;            
        case "latest":
            $pageTitleText = "Latest";
            $pageTitle = $pageTitleText . " - " . $companyName . " - " . $pageTitleExtra;
            $pageMetaDescription = "Stay up to date with everything Nightlife Music. You can find our latest newsletters, blogs and thoughts right here.";
            break;
        case "careers":
            $pageTitleText = "Careers";
            $pageTitle = $pageTitleText . " - " . $companyName . " - " . $pageTitleExtra;
            $pageMetaDescription = "Innovation, ingenuity and team work. If that sounds like a good day at work to you then you’ll love Nightlife Music. ";
            break;            
        case "login":
            $pageTitleText = "Login";
            $pageTitle = $pageTitleText . " - " . $companyName . " - " . $pageTitleExtra;
            $pageMetaDescription = "Hey there Nightlifer! Log in here to access our client resources and information. If you need any help, remember you can call us anytime on 1800 773 468.";
            break;
        case "contact":
            $pageTitleText = "Contact";
            $pageTitle = $pageTitleText . " - " . $companyName;
            $pageMetaDescription = "Ready to bring your venue to life? We would love to talk to you! Drop us an email or call us on 1800 679 748.";
            break;
        case "charts":
            $pageTitleText = "Charts";
            $pageTitleExtra = "Live song plays from Nightlife systems around the country.";
            $pageTitle = $pageTitleText . " - " . $companyName . " - " . $pageTitleExtra;
            $pageMetaDescription = "Now you can listen to the music being played inside Australia's best venues!";
            break;         
        case "mmn":
            $pageTitleText = "Manage My Nightlife";
            $pageTitleExtra = "You have total control.";
            $pageTitle = $pageTitleText . " - " . $companyName . " - " . $pageTitleExtra;
            $pageMetaDescription = "Whether you are on your smartphone, tablet or desktop you have total control.";
            break; 
        case "crowddj":
            $pageTitleText = "crowdDJ";
            $pageTitleExtra = "Let your customers have a say in what you play.";
            $pageTitle = $pageTitleText . " - " . $companyName . " - " . $pageTitleExtra;
            $pageMetaDescription = "crowdDJ is the first of our consumer apps that lets your music become a talking point in your venue to help reinforce your brand identity and make better connections with your customers.";
            break; 
        case "enterprise":
            $pageTitleText = "Nightlife Enterprise";
            $pageTitleExtra = "Manage your brand's advertisements, insights and controls to help you manage every site from one spot.";
            $pageTitle = $pageTitleText . " - " . $companyName . " - " . $pageTitleExtra;
            $pageMetaDescription = "Whether you have 1 site or 1,000 sites, we help you deliver a consistent brand and message across all of them.";
            break;            
        case "page404":
            $pageTitleText = "404 Error";
            $pageTitle = $pageTitleText . " - " . $companyName . " - " . $pageMetaDescription;
            $pageMetaDescription = "Oh no! It looks like there has been an error. We can still help you – come back to the home page or call 1800 679 748 if you still can’t find what you’re looking for!";
            break;
        default:
            $pageTitleText = "Nightlife Music";
            $pageTitle = $pageTitleText . " - " . $companyName . " - " . $pageTitleExtra;
            $pageMetaDescription = "Bring Your Venue To Life!";
            break;
    }
}

function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
} 

function getDirectory( $path = '.', $level = 0 ){
    // Read PDF files in directory
    // Used for the Careers page 

    $ignore = array( 'cgi-bin', '.', '..' ); 
    // Directories to ignore when listing output. Many hosts 
    // will deny PHP access to the cgi-bin. 

    $dh = @opendir( $path ); 
    // Open the directory to the handle $dh 

    $retStr = false;
    
    while( false !== ( $file = readdir( $dh ) ) ){ 
    // Loop through the directory 
        if($debug){ echo "<pre style='border: 1px solid blue;'>in while: {$retStr}</pre>"; }
        if( !in_array( $file, $ignore) ){ // Check that this file is not to be ignored or a directory
            $retStr .= "<li class='resourceslist'><a href='$path/$file' target='_blank'> $file</a></li>";
        }
    }
    
    if($retStr == false){
        $retStr = "<ul id=\"nojobs\"><li>There are currently no jobs available at Nightlife. Please feel free to send your resume to keep on file.</li></ul>";
        if($debug){ echo "<pre style='border: 1px solid blue;'>out while: {$retStr}</pre>"; }
    }
     
    closedir( $dh ); 
    // Close the directory handle 
    return $retStr;
} 


// Set up Some Title, Meta Tags etc for $page
setPageDetails($page);

?>