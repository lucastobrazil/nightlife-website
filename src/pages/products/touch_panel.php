<section id="mmn" class="row text-center" style="background-image: url('img/what_we_do/products/nightlife_kiosk.jpg'); color: white;">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-2">
		<div class="col-lg-8 col-lg-offset-2">
		    <i class="nm-icons-lm-juke fa-4x"></i>
		    <h2>Touch Panel Kiosk</h2>
		    <hr class="colored">
		</div>
		<div class="col-lg-8 col-lg-offset-2">
		    <p>Our Touch Panels and Kiosks are a sleek and compact take on the jukebox; letting your customers interact with your music on your terms. </p>
			<p>You control what music your customers have access to and you can tailor the selection for special events making this the perfect solution for your function room.</p>
			<p>Gyms are one of the biggest users of our touch panels. The friendly design gets their members picking songs and involved in the atmosphere. Our gym clients report more engagement and less headphones. Which sets the perfect scene for better interactions and relationship building with staff. </p>
		</div>
	</div>
</section>