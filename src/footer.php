    <section id="sitemap">
        <div class="container text-center">
            <!-- Removed until we sort out the Swiftype search plugin
            <form id="search-bottom" class="hidden-lg col-xs-8 col-xs-offset-2" action="#" method="post">
                <span class="col-xs-12">
                    <input type="text" class="form-control col-xs-7" name="search-terms" id="search-terms" placeholder="Search this site..." />
                    <button class="btn btn-outline-light col-xs-2">Search</button>
                </span>
            </form>
        --> 

            <div class="col-xs-12 col-sm-4">
                <ul>   
                    <h3>What We Do</h3>
                    <li><a href="<?php echo $what_we_do; ?>#music">Music</a></li>
                    <li><a href="<?php echo $what_we_do; ?>#visuals">Visuals</a></li>
                    <li><a href="<?php echo $what_we_do; ?>#digitalsignage">Digital Signage</a></li>
                    <li><a href="<?php echo $what_we_do; ?>#licensing">Licensing</a></li>
                    <li><a href="<?php echo $what_we_do; ?>#products">Products &amp; Features</a></li>
                    <li><a href="<?php echo $faq; ?>">FAQ</a></li>
                </ul>
                <ul>
                    <h3>Case Studies</h3>
                    <li><a href="<?php echo $case_studies; ?>">Testimonials</a></li>
                </ul>
                <ul>
                    <h3>Latest</h3>
                    <li><a href="../blog">Blog</a></li>
                    <li>Media</li>
                    <li>Resources</li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4">
                <ul>
                    <h3>Our Story</h3>
                    <li><a href="<?php echo $our_story; ?>">About Nightlife Music</a></li>
                    <li><a href="<?php echo $our_story; ?>#community">Community</a></li>
                    <li><a href="<?php echo $careers; ?>">Careers</a></li>
                </ul>
                <ul>
                    <h3>Contact Us</h3>
                    <li><a href="<?php echo $contact; ?>">Email</a></li>
                    <li><a href="<?php echo $contact; ?>">Phone</a></li>
                    <li><a href="<?php echo $contact; ?>">Nightlife HQ</a></li>
                    <li>
                </ul>
                <ul>
                    <h3>Clients</h3>
                    <li><a href="<?php echo $login; ?>">Login</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4">
                <ul>
                    <h3>Apps</h3>
                    <li><a href="<?php echo $what_we_do; ?>#products"><i class="nm-icons-crowddj"></i> crowdDJ</a></li>
                    <li><a href="<?php echo $what_we_do; ?>#products"><i class="nm-icons-mmn"></i> Manage My Nightlife</a></li>
                </ul>
            </div>                                
            <div class="col-xs-12 col-sm-4">
                <ul>
                    <li>
                        <a href="http://www.nightlife.com.au/privacy_policy/nightlife_website_privacy_policy.html">
                            Privacy Policy
                        </a>
                </ul>
            </div>                                
        </div>
</section>
<footer class="footer" style="background-image: url('img/bg-footer.jpg')">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-4 contact-details">
                <h4><i class="fa fa-phone"></i> Call Us Today</h4>         
                <p>Enquiries: 1800 679 748</p>
                <p>Service: 1800 773 468</p>
            </div>
            <div class="col-md-4 contact-details">
                <h4><i class="fa fa-map-marker"></i> Visit</h4>
                <p>50 Cribb St, Milton
                    <br>QLD Australia 4064</p>
            </div>
            <div class="col-md-4 contact-details">
                <h4><i class="fa fa-envelope"></i> Email</h4>
                <p><a href="mailto:info@nightlife.com.au">info@nightlife.com.au</a>
                </p>
            </div>
        </div>
        <div class="row social-bar">
            <div class="col-lg-12">
                <ul class="list-inline">
                    <li>
                        <a href="https://twitter.com/nightlifemusic" target="_blank">
                        <svg version="1.1" id="social_twitter" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve">
                            <path fill="#ffffff" d="M0,0v200.1h200.4V0.4L0,0z M165.4,69.6c0.1,1.3,0.1,2.6,0.1,4c0,40.5-30.8,87.2-87.2,87.2
                                c-17.3,0-33.4-5.1-47-13.8c2.4,0.3,4.8,0.4,7.3,0.4c14.4,0,27.6-4.9,38.1-13.1c-13.4-0.2-24.7-9.1-28.6-21.3
                                c1.9,0.4,3.8,0.5,5.8,0.5c2.8,0,5.5-0.4,8.1-1.1c-14-2.8-24.6-15.2-24.6-30.1c0-0.1,0-0.3,0-0.4c4.1,2.3,8.9,3.7,13.9,3.8
                                C42.9,80.4,37.5,71,37.5,60.4c0-5.6,1.5-10.9,4.2-15.4c15.1,18.5,37.7,30.7,63.2,32c-0.5-2.2-0.8-4.6-0.8-7
                                c0-16.9,13.7-30.7,30.7-30.7c8.8,0,16.8,3.7,22.4,9.7c7-1.4,13.5-3.9,19.5-7.4c-2.3,7.2-7.1,13.2-13.5,17c6.2-0.7,12.1-2.4,17.6-4.8
                                C176.6,59.9,171.4,65.3,165.4,69.6z"/>
                        </svg>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/NightlifeMusicOfficial" target="_blank">
                            <svg version="1.1" id="social_facebook" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve">
                                <path fill="#ffffff" d="M0,0.7v199.8l200,0V0L0,0.7z M169.9,43.4l-15.9,0c-12.5,0-14.9,5.9-14.9,14.6v19.2h29.7l-3.9,30h-25.8v77
                                    h-31v-77H82.2v-30h25.9V55c0-25.7,15.7-39.6,38.6-39.6c11,0,20.4,0.8,23.1,1.2V43.4z"/>
                            </svg>
                        </a>
                    </li>
                    <li>
                        <a href="https://instagram.com/nightlifemusic" target="_blank">
                            <svg version="1.1" id="social_instagram" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve">
                                <path fill-rule="evenodd" clip-rule="evenodd" fill="#020000" d="M178.5,84.5h-17.9c1.3,5.1,2.1,10.3,2.1,15.8
                                    c0,34.9-28.3,63.2-63.1,63.2c-34.9,0-63.1-28.3-63.1-63.2c0-5.5,0.8-10.7,2.1-15.8H20.6v86.8c0,4.4,3.5,7.9,7.9,7.9h142.1
                                    c4.4,0,7.9-3.5,7.9-7.9V84.5z M178.5,29.3c0-4.4-3.5-7.9-7.9-7.9h-23.7c-4.4,0-7.9,3.5-7.9,7.9v23.7c0,4.4,3.5,7.9,7.9,7.9h23.7
                                    c4.4,0,7.9-3.5,7.9-7.9V29.3z M99.5,60.8c-21.8,0-39.5,17.7-39.5,39.5c0,21.8,17.7,39.5,39.5,39.5c21.8,0,39.5-17.7,39.5-39.5
                                    C139,78.5,121.3,60.8,99.5,60.8 M200,199.9L0,199.8V0l200,0.1"/>
                            </svg>
                        </a>
                    </li>
                </ul>


            </div>
        </div>
        <div class="row copyright">
            <div class="col-lg-12">
                <p class="small">&copy; 2015 Nightlife Music Pty Ltd</p>
            </div>
        </div>
    </div>
</footer>

<!-- Core Scripts -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<!-- Plugin Scripts -->
<script src="js/plugins/jquery.easing.min.js"></script>
<script src="js/plugins/jquery.cycle.all.latest.js"></script>
<script src="js/plugins/classie.js"></script>
<?php if($page ==='home'){
    echo '<script src="js/plugins/cbpAnimatedHeader.js"></script>';
}
?>


<script src="js/plugins/owl-carousel/owl.carousel.js"></script>
<script src="js/plugins/jquery.magnific-popup/jquery.magnific-popup.min.js"></script>
<!--<script src="js/plugins/jquery.fs.wallpaper.js"></script>-->
<script src="js/plugins/background/core.js"></script>
<script src="js/plugins/background/transition.js"></script>
<script src="js/plugins/background/background.js"></script>
<script src="js/plugins/jquery.mixitup.js"></script>
<script src="js/plugins/wow/wow.min.js"></script>
<script src="js/contact_me.js"></script>
<script src="js/plugins/jqBootstrapValidation.js"></script>
<script src="js/plugins/bootstrap-hover-dropdown.min.js"></script>
<script src="js/plugins/bootstrap-tabexpand.js"></script>

<!-- Vitality Theme Scripts -->
<script src="js/vitality.js"></script>
<script src="js/plugins/nightlife.tweet.cycle.js"></script>

  <script>
/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
function detectIE() {
    var ua = window.navigator.userAgent;
    alert(ua);
    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        var version =parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        if (version < 8){
            //return 'OLD!';
            $('html').html('ie sucks');
            return true;
        }else{
            return 'okay';
        }
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       // IE 12 => return version number
       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}

//detectIE();

$(document).ready(function(){
    setModalVideoSize($('#genoverview_video'));
    setModalVideoSize($('#infographic_video'));

    $('.homevideo').hover(function(){
        $(this).find('i').toggleClass('bounceIn pulse infinite');
    });

    $('#introvideo').hover(function(){
        $(this).find('.play-btn').toggleClass('bounceIn pulse infinite');
    });

    $('.collapsible-tabs').tabExpand();

    // When users on iPhone close the fullscreen video, dismiss the modal 
    $('video').on('webkitendfullscreen', function(e) {
        $(this).closest('.video-modal').modal('hide');
    });

});

$(window).resize(function(){
    setTimeout(function(){
        setModalVideoSize($('#genoverview_video'));
        setModalVideoSize($('#infographic_video'));
    }, 500);
});


$('#videoModal').on('show.bs.modal', function (e) {
    setModalVideoSize($('#infographic_video'));
    $('#infographic_video')[0].play();
}).on('hidden.bs.modal', function (e) {
    $('#infographic_video')[0].pause();
});
$('#homeVideoModal').on('show.bs.modal', function (e) {
            setModalVideoSize($('#genoverview_video'));
    $('#genoverview_video')[0].play();
}).on('hidden.bs.modal', function (e) {
    $('#genoverview_video')[0].pause();
});

// jQuery('.portfolio-modal').on('show.bs.modal', function(e){
//     // var elemID = $(e.target).attr('id');
//     // var target =  elemID.substr(0, elemID.length - 6);
//     // var url = window.location.href + "&" + target;
//     // window.location.href = url;
// }).on('hide.bs.modal', function(e){
//     // var elemID = $(e.target).attr('id');
//     // //var targetLen =  elemID.substr(0, elemID.length - 6);
//     // var url = window.location.pathname;
//     // var origUrl = url.substr(0, url.length - elemID.length);
//     // window.location.href = url;
// });


(function(window){
    // get vars
    var searchEl = document.querySelector("#input");
    var labelEl = document.querySelector("#label");

    // register clicks and toggle classes
    labelEl.addEventListener("click",function(){
        if (classie.has(searchEl,"focus")) {
            classie.remove(searchEl,"focus");
            classie.remove(labelEl,"active");
        } else {
            classie.add(searchEl,"focus");
            $('#input input').focus();
            classie.add(labelEl,"active");
        }
    });

    // register clicks outisde search box, and toggle correct classes
    document.addEventListener("click",function(e){
        var clickedID = e.target.id;
        if (clickedID != "search-terms" && clickedID != "search-label") {
            if (classie.has(searchEl,"focus")) {
                classie.remove(searchEl,"focus");
                classie.remove(labelEl,"active");
            }
        }
    });
}(window));

</script>
</body>
</html>
