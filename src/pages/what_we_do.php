<section id="whatwedo-topbar" class="blue">
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-lg-12 wow fadeIn">
                <h1>We capture your Brand and translate it into sound and vision</h1>
                <p>It’s music and visuals designed for you and delivered as a complete commercial entertainment system.</p>
            </div>
        </div>
    </div>
</section>
<section id="whatwedo">
    <div class="col-xs-12 text-center wwd-stamp">
        <img src="img/what_we_do_sign.png" class="img-responsive" />
    </div>
    <div class="row text-center wwd-grid" style="padding:20px;">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <span>
                    <h3>Music</h3>
                    <p>Sometimes it’s what gets left out that makes the difference.</p>
                    <a class="page-scroll"  href="#music">
                        <button class="btn btn-primary">More on Music <i class="fa fa-angle-down"></i></button>
                    </a>
                </span>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <span>
                    <h3>Visuals</h3>
                    <p>From the beaches of Aruba to the streets of Sao Paulo; there is always more to see.</p>
                    <a href="#visuals" class="page-scroll">
                        <button class="btn btn-success">More <span class="hidden-xs">on Visuals</span> <i class="fa fa-angle-down"></i></button>
                    </a>
                </span>
            </div>
            <div class="col-xs-12 col-sm-6  col-md-4">
                <span>
                    <h3>Digital Signage</h3>
                    <p>Full integration, full power. Your ads and promotions delivered the way you want.</p>
                    <a href="#digitalsignage" class="page-scroll">
                        <button class="btn btn-primary">More <span class="hidden-xs">on Digital Signage</span>  <i class="fa fa-angle-down"></i></button>
                    </a>
                </span>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <span>
                    <h3>Licensing</h3>
                    <p>Let our experts take the stress and the hassle out of your venue’s licensing.</p>
                    <a href="#licensing" class="page-scroll">
                        <button class="btn btn-success">More <span class="hidden-xs">on Licensing</span>  <i class="fa fa-angle-down"></i></button>
                    </a>
                </span>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <span>
                    <h3>Products &amp; Features</h3>
                    <p>Market leading technology and apps helping you get social and take control, wherever you are.</p>
                    <a href="#products" class="page-scroll">
                        <button class="btn btn-primary">More <span class="hidden-xs">on Products</span>  <i class="fa fa-angle-down"></i></button>
                    </a>
                </span>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <span>
                    <h3>FAQ</h3>
                    <p>Got a question? Give us a call or check out the FAQs!</p>
                    <a href="faq">
                        <button class="btn btn-success">See All  <i class="fa fa-angle-right"></i></button>
                    </a>
                </span>
            </div>
        </div>
    </div>        
</section>
<section id="music">
<div class="container-fluid">
    <div class="row text-center">
        <div class="col-lg-12 wow fadeIn" style="margin-top: -150px;">
            
            <div class="cardfan hidden-xs col-sm-12 col-md-4">
              <img src="img/album_art/aretha_franklin_queen_of_soul.jpg" alt="Aretha Franklin - Queen of Soul">
              <img src="img/album_art/bruce_springsteen_born_in_the_usa.jpg" alt="Bruce Springsteen - Born in the USA">
              <img src="img/album_art/radiohead_kid_a.jpg" alt="Radiohead - Kid A">
            </div>
           
            <h1 class="col-xs-12 col-sm-12 col-md-4" style="margin-top: 130px;">Music</h1>
            
            <div class="cardfan hidden-xs hidden-sm col-md-4">
              <img src="img/album_art/de_la_soul_3_feet_high_and_rising.jpg" alt="De La Soul - 3 Feet High and Rising">
              <img src="img/album_art/michael_jackson_dangerous.jpg" alt="Michael Jackson - Dangerous">
              <img src="img/album_art/joy_division_unknown_pleasures.jpg" alt="Joy Division - Unknown Pleasures">
            </div>
                           
            <span class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-4 col-md-offset-4">
                <p>With over 40 million songs in the world sometimes the hardest part is working out what you don’t want to play. We take the hard work out of it. We find out what makes your business tick and then we design your music to match. Creating a library of music that is perfect for you. </p>
                <h3>Here are just some of the top comments we hear from new clients:</h3>
                <hr class="colored">
            </span>
        </div>
    </div>
    <div class="row text-center content-row">
        <div class="row text-center content-row icon-columns">
            <div class="col-sm-6 col-md-4 col-lg-2 col-lg-offset-1 wow fadeIn" data-wow-delay=".2s">
                <div class="about-content animatedOnHover">
                    <i class="fa fa-star fa-4x animated pulse infinite"></i>
                    <h3>Music is everything</h3>
                    <p class="quote">"Our customers are discerning - we need a feature playlist that stays ahead of the crowd."</p>
                    <p>We ensure your music connects with your clientele plus you have the technology to take control.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-2 wow fadeIn" data-wow-delay=".4s">
                <div class="about-content animatedOnHover">
                    <i class="fa fa-refresh fa-4x animated rotateIn infinite"></i>
                    <h3>Music that stays fresh </h3>
                    <p class="quote">"We tried finding our own music but there’s too much to go through!"</p>
                    <p>We do the hard work - finding you the right songs and creating you unique playlists, every time.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-2  wow fadeIn" data-wow-delay=".6s">
                <div class="about-content animatedOnHover">
                    <i class="fa fa-volume-up fa-4x animated pulse infinite"></i>
                    <h3>Music you can turn up  </h3>
                    <p class="quote">"We need perfect sound quality; from background music to a party bar." </p>
                    <p>Every song is remastered in house so you can trust our DJ quality sound.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-2 wow fadeIn" data-wow-delay=".8s">
                <div class="about-content animatedOnHover">
                    <i class="fa fa-heart fa-4x animated pulse infinite"></i>
                    <h3>Music that looks after itself </h3>
                    <p class="quote">"We don’t have time to manage the music – it just needs to work."</p>
                    <p>We schedule your music down to the minute with everything from announcements to volume changes.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-2 wow fadeIn" data-wow-delay="1s">
                <div class="about-content animatedOnHover">
                    <i class="fa fa-cutlery fa-4x animated pulse infinite"></i>
                    <h3>Music for dining  </h3>
                    <p class="quote">"We need restaurant music but we don’t want dodgy covers or unknown tracks."</p>
                    <p>Our PPC solution is original music by the best artists, remastered for perfect quality.</p>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<a name="visuals"></a>
<section id="visuals">
    <div class="container text-center">
        <h1>Visuals</h1>
        <h3>The best artists from around the globe creating living wallpaper for your venue.</h3>
        <p>We hand-pick the clips that will engage your customers so that when you’re ready to share your promotional messages, your customers are ready to act. </p>
    </div>
    <div class="standard-carousel amv-carousel wow fadeIn">
        <div class="item" style="background-image: url('img/amv/ambient_visuals_fashion.jpg'); color: black;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="project-details">
                            <span class="project-name col-xs-12">Luxe and Fashion </span>
                            <span class="project-description col-xs-12">Looking for a taste of the finer things in life? Our luxe and fashion content are a stunning backdrop with the latest high fashion and luxury lifestyle clips.</span>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item adrenaline">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="project-details">
                            <span class="project-name col-xs-12">Adrenaline and Action</span>
                            <span class="project-description col-xs-12">If human slingshots, base jumping, trike drifting or zorb soccer are your kind of thing then we’ve got you covered with awesome clips from big names like Devin Super Tramp.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item" style="background-image: url('img/amv/ambient_visuals_travel.jpg'); color: rgb(114, 69, 20)">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="project-details">
                            <span class="project-name col-xs-12">Life and Travel </span>
                            <span class="project-description col-xs-12">Create a whole new world in your venue and escape to the beaches of Aruba or trek through the mountains to Machu Pichu with incredible footage from around the globe. </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item" style="background-image: url('img/portfolio/bg-4.jpg')">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="project-details">
                            <span class="project-name col-xs-12">Vintage Film </span>
                            <span class="project-description col-xs-12">Add a touch of nostalgia and vintage class with clips from classic films like Breakfast at Tiffany’s.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item" style="background-image: url('img/amv/ambient_visuals_digital_art.jpg')">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="project-details">
                            <span class="project-name col-xs-12">Digital Art</span>
                            <span class="project-description col-xs-12">Globules of pulsating octa-mesh are a real thing and they are part of our digital art library that will transform your screens and dance floor.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item" style="background-image: url('img/portfolio/bg-5.jpg'); color: rgb(33, 24, 43);">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="project-details">
                            <span class="project-name col-xs-12">Music Videos</span>
                            <span class="project-description col-xs-12">The most memorable visuals are the ones designed specifically for that song. More and more music videos are a perfect example of what happens when music and art collide.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<section id="digitalsignage" class="services">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-12 wow fadeIn">
                <h2>Digital Signage</h2>
                <hr class="colored">
                <h3>Make your promotions powerful</h3>
                <img src="img/da/digital_advertising_screen.jpg" class="img-responsive img-centered" />
                <p>Our integrated Digital Signage platform ensures your promotions have maximum impact by weaving your advertising and messages with engaging visuals.</p>
                <p>It’s easy to manage your ads across 1 or 100 sites from anywhere with our web app. Load your own files or create new ads using easy inbuilt templates and for quick news style updates use scrolling text. Nightlife gives you total control of your screens.</p>
            </div>
        </div>
    </div>
</section>

<section class="blue" id="licensing">
    <div class="container text-center wow fadeIn">
        <div class="row">
            <div class="col-md-8 col-lg-12">
                <h2>Licensing</h2>
                <p>Simplify your licensing obligations with expert advice. From venues with full performance requirements to restaurants looking for background music and PPC solutions, we have all your bases covered.</p>
                <p><a href="<?php echo $faq; ?>#licensing"><button class="btn btn-success">Read our Licensing FAQs  <i class="fa fa-angle-right"></i></button></a></p>
            </div>
        </div>
    </div>
</section>
<section id="products">
    <div class="container text-center wow fadeIn">
        <h2>Products &amp; Features</h2>
        <hr class="colored">
        <p>From apps to Enterprise, Instagram to karaoke and jukeboxes to bingo – we’ve got more features than you can poke a stick at.</p>
        <div class="row content-row">
            <div class="col-lg-12">
                <div class="products-filter">
                    <ul id="filters" class="clearfix">
                        <li>
                            <span class="filter active" data-filter="apps features hardware businesstools">All</span>
                        </li>
                        <li>
                            <span class="filter" data-filter="features">Features</span>
                        </li>
                        <li>
                            <span class="filter" data-filter="apps">Apps</span>
                        </li>
                        <li>
                            <span class="filter" data-filter="businesstools">Business Tools</span>
                        </li>
                        <li>
                            <span class="filter" data-filter="hardware">Hardware</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div id="productsgrid">
                    <div class="portfolio apps businesstools" data-cat="apps businesstools">
                        <a href="mmn" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/manage_my_nightlife.jpg" alt="Manage My Nightlife">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="nm-icons-mmn"></i> Manage My Nightlife</span>
                                        <p>Control the music, visuals and configure the Nightlife system in your business.</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>    
                    </div>
                    <div class="portfolio features" data-cat="features">
                        <a href="scheduler" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/portfolio/grid/identity/2.jpg" alt="">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="fa fa-calendar"></i> Scheduler</span>
                                        <p>Control the music, visuals and configure the Nightlife system in your business.</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="portfolio businesstools" data-cat="businesstools">
                        <a href="enterprise" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/nightlife_enterprise.jpg" alt="Nightlife Enterprise">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="nm-icons-music-zone"></i> Nightlife Enterprise</span>
                                        <p>Manage the music and visual content for your entire business or group from one simple app.</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="portfolio apps" data-cat="apps">
                        <a href="crowddj" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/crowddj.jpg" alt="crowdDJ">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title" href="crowddj"><i class="nm-icons-crowddj"></i> crowdDJ</span>
                                        <p>crowdDJ lets your customers have a say in what you play right from their own smart phone.  </p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div> 
                    <div class="portfolio features" data-cat="features">
                        <a href="instagram" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/instagram_integration.jpg" alt="Nightlife's integration with Instagram'">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="fa fa-instagram"></i> Instagram Integration</span>
                                        <p>Respond to trends on-the-go and display an Instagram feed on your screens.</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="portfolio features" data-cat="features">
                        <a href="karaoke" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/nightlife_karaoke.jpg" alt="Nightlife Karaoke'">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="fa fa-microphone"></i> Karaoke</span>
                                        <p>Engage your customers and get them singing! You’ll have everything from the greatest classics to the latest hits.</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="portfolio hardware" data-cat="hardware">
                        <a href="nightlife-system" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/nightlife_system.jpg" alt="The Nightlife System">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="nm-icons-lm-juke"></i> The Nightlife System</span>
                                        <p>Turn Your Entertainment Into Revenue. You can control what music is available when!</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>  
                    <div class="portfolio hardware" data-cat="hardware">
                        <a href="nightlife-microsystem" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/nightlife_microsystem.jpg" alt="The Nightlife Micro System">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="nm-icons-lm-juke"></i> The Nightlife Microsystem</span>
                                        <p>Turn Your Entertainment Into Revenue. You can control what music is available when!</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>   
                    <div class="portfolio hardware" data-cat="hardware">
                        <a href="ppc-solution" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/ppc_solution.jpg" alt="PPC Solution for Restaurants">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="nm-icons-lm-juke"></i> PPC Solution for Restaurants</span>
                                        <p>Turn Your Entertainment Into Revenue. You can control what music is available when!</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>                                        
                    <div class="portfolio hardware" data-cat="hardware">
                        <a href="jukebox" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/nightlife_jukebox.jpg" alt="Nightlife Jukebox">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="nm-icons-lm-juke"></i> Jukebox</span>
                                        <p>Turn Your Entertainment Into Revenue. You can control what music is available when!</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>                    
                    <div class="portfolio hardware" data-cat="hardware">
                        <a href="touch-panel" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/nightlife_kiosk.jpg" alt="Nightlife Kiosk'">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="fa fa-tablet"></i> Touch Panel Kiosk</span>
                                        <p>Perfect for your function room! Let your customers interact with your music on your terms.</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="portfolio features" data-cat="features">
                        <a href="random-number-generator" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/nightlife_random_number_generator.jpg" alt="Nightlife Random Number Generator'">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="fa fa-gift"></i> Random Number Generator</span>
                                        <p>Control the music, visuals and configure the Nightlife system in your business.</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="portfolio features" data-cat="features">
                        <a href="bingo" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/nightlife_bingo.jpg" alt="Nightlife Bingo'">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="fa fa-gamepad"></i> Bingo</span>
                                        <p>An instant classic. Set up games and control them from your device!</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="portfolio features" data-cat="features">
                        <a href="scrolling-text" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/scrollingtext.jpg" alt="Nightlife Scrolling Text'">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="fa fa-text-width"></i> Scrolling text</span>
                                        <p>Quickly send messages to your customers without affecting the vibe.</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="portfolio features" data-cat="features">
                        <a href="picture-gallery" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/picture_gallery.jpg" alt="Nightlife Picture Gallery'">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="fa fa-calendar"></i> Picture Gallery</span>
                                        <p>Control the music, visuals and configure the Nightlife system in your business.</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>                    
                    <div class="portfolio businesstools" data-cat="businesstools">
                        <a href="advertising-management" target="_self">
                            <div class="portfolio-wrapper">
                                <img src="img/what_we_do/products_grid/advertising_management.jpg" alt="Advertising Management">
                                <div class="caption">
                                    <div class="caption-text">
                                        <span class="text-title"><i class="nm-icons-visual-content"></i>  Advertising Management</span>
                                        <p>Control the music, visuals and configure the Nightlife system in your business.</p>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
