<section id="crowddj" class="row" style="background-image: url('img/what_we_do/products/crowddj_bg.jpg'); color: white;">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-10 col-md-offset-1">
		<div class="col-lg-8 col-lg-offset-2 text-center" >
		    <img src="img/icons/crowd_dj_icon.jpg" class="img-responsive img-centered" alt="">
		    <h2>crowdDJ</h2>
		    <h3>Looking for the next big thing to engage your customers?</h3>
		    <p>crowdDJ lets your customers have a say in what you play right from their own smart phone.</p>
		    <hr class="colored">
		</div>
		<div class="col-lg-8 col-lg-offset-2">
		    <p>crowdDJ is the first of our consumer apps that lets your music become a talking point in your venue to help reinforce your brand identity and make better connections with your customers. </p>
		    <p>Once your customers download crowdDJ they can check out your playlist, search for their own favourite songs and request them to be played. </p>
		    <p>You can enable crowdDJ interaction and set limits around what music your customers can access to ensure that you always stay in control. </p>
		</div>
		<div class="col-sm-8 col-sm-offset-2">
			<div class="col-xs-12 col-sm-6">
			    <a href="https://play.google.com/store/apps/details?id=com.nightlife.crowdDJ" target="_blank">
			        <img src="img/icons/android_app_on_google_play.png" class="img-responsive" />
			    </a>
			</div>
			<div class="col-xs-12 col-sm-6">
			    <a href="https://itunes.apple.com/us/app/crowddj/id911666442?ls=1&mt=8" target="_blank">
			        <img src="img/icons/available_on_the_app_store.png" class="img-responsive"  />
			    </a>
			</div>
		</div>
	</div>
</section>