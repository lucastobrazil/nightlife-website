<section id="mmn" class="row text-center" style="background-image: url('img/what_we_do/products/picture_gallery.jpg'); color: white;">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-2">
		<div class="col-lg-8 col-lg-offset-2">
            <i class="fa fa-camera fa-4x"></i>
            <h2>Picture Gallery</h2>
            <hr class="colored">
        </div>
        <div class="col-lg-8 col-lg-offset-2">
            <p>Our inbuilt picture gallery makes it easy to load and display your photos on your screens. Whether you have a private function, event photos or social photographer this is a great way to let your customers see themselves as part of your business. </p>
        </div>
	</div>
</section>