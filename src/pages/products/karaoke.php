<section id="mmn" class="row text-center"style="background-image: url('img/what_we_do/products/nightlife_karaoke.jpg'); color: white;">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-2">
		<div class="col-lg-8 col-lg-offset-2">
            <i class="fa fa-microphone fa-4x"></i>
            <h2>Karaoke</h2>
            <hr class="colored">
        </div>
        <div class="col-lg-8 col-lg-offset-2">
            <p>Nothing mixes quite as well as some liquid courage and a karaoke night! We handpick every one of our Karaoke songs, just like the rest of our extensive library so you can rest assured you’ll have everything from the greatest classics to the latest hits. </p>
            <p>With your Nightlife System already integrated into your AV system Karaoke is easy to switch to and it can be controlled via Manage My Nightlife. This gives total mobility and flexibility for your host or you can make it easy for your customers to control their own selections and party with crowdDJ. </p>
        </div>
	</div>
</section>