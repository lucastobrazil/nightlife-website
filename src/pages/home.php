    
    <header class="video">
        <div class="overlay"></div>
        <div class="intro-content">
            <div class="brand-name">Harness the Power of Music</div>
            <div class="brand-name-subtext">
                <span>It's your music, your vision and your identity. Delivered with full control, powerful automation and expert support.</span>

                <div class="homevideo col-xs-10 col-xs-offset-1 col-lg-4 col-lg-offset-4" data-cat="web" href="#homeVideoModal"  data-toggle="modal">
                    <i class="fa fa-play-circle "></i>
                </div>
            </div>
        </div>
        <div class="homeslide-footer">
            <!--<div class="scroll-down">
                <a class="btn page-scroll pulse animated infinite" href="#introvideo"><i class="fa fa-angle-down"></i></a>
            </div>-->
            <div class="social-bar filled">
                <div class="hidden-sm col-md-3"> </div>
                <div class="col-xs-12 col-sm-9 col-md-6">
                    <?php
                        //if on a mobile, don't show the tweets stream
                        if(!isMobile()){
                            include('php/tweets.php');
                        }
                    ?>
                </div>
                <div class="hidden-xs col-sm-3 col-md-3">
                    <a href="https://twitter.com/nightlifemusic" target="_blank">
                        <svg version="1.1" id="social_twitter" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve">
                            <path fill="#ffffff" d="M0,0v200.1h200.4V0.4L0,0z M165.4,69.6c0.1,1.3,0.1,2.6,0.1,4c0,40.5-30.8,87.2-87.2,87.2
                                c-17.3,0-33.4-5.1-47-13.8c2.4,0.3,4.8,0.4,7.3,0.4c14.4,0,27.6-4.9,38.1-13.1c-13.4-0.2-24.7-9.1-28.6-21.3
                                c1.9,0.4,3.8,0.5,5.8,0.5c2.8,0,5.5-0.4,8.1-1.1c-14-2.8-24.6-15.2-24.6-30.1c0-0.1,0-0.3,0-0.4c4.1,2.3,8.9,3.7,13.9,3.8
                                C42.9,80.4,37.5,71,37.5,60.4c0-5.6,1.5-10.9,4.2-15.4c15.1,18.5,37.7,30.7,63.2,32c-0.5-2.2-0.8-4.6-0.8-7
                                c0-16.9,13.7-30.7,30.7-30.7c8.8,0,16.8,3.7,22.4,9.7c7-1.4,13.5-3.9,19.5-7.4c-2.3,7.2-7.1,13.2-13.5,17c6.2-0.7,12.1-2.4,17.6-4.8
                                C176.6,59.9,171.4,65.3,165.4,69.6z"/>
                        </svg>
                    </a>
                    <a href="https://www.facebook.com/NightlifeMusicOfficial" target="_blank">
                        <svg version="1.1" id="social_facebook" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve">
                            <path fill="#ffffff" d="M0,0.7v199.8l200,0V0L0,0.7z M169.9,43.4l-15.9,0c-12.5,0-14.9,5.9-14.9,14.6v19.2h29.7l-3.9,30h-25.8v77
                                h-31v-77H82.2v-30h25.9V55c0-25.7,15.7-39.6,38.6-39.6c11,0,20.4,0.8,23.1,1.2V43.4z"/>
                        </svg>
                    </a>
                    <a href="https://instagram.com/nightlifemusic" target="_blank">
                        <svg version="1.1" id="social_instagram" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve">
                            <path fill-rule="evenodd" clip-rule="evenodd" fill="#020000" d="M178.5,84.5h-17.9c1.3,5.1,2.1,10.3,2.1,15.8
                                c0,34.9-28.3,63.2-63.1,63.2c-34.9,0-63.1-28.3-63.1-63.2c0-5.5,0.8-10.7,2.1-15.8H20.6v86.8c0,4.4,3.5,7.9,7.9,7.9h142.1
                                c4.4,0,7.9-3.5,7.9-7.9V84.5z M178.5,29.3c0-4.4-3.5-7.9-7.9-7.9h-23.7c-4.4,0-7.9,3.5-7.9,7.9v23.7c0,4.4,3.5,7.9,7.9,7.9h23.7
                                c4.4,0,7.9-3.5,7.9-7.9V29.3z M99.5,60.8c-21.8,0-39.5,17.7-39.5,39.5c0,21.8,17.7,39.5,39.5,39.5c21.8,0,39.5-17.7,39.5-39.5
                                C139,78.5,121.3,60.8,99.5,60.8 M200,199.9L0,199.8V0l200,0.1"/>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </header>

    <section id="introvideo" data-cat="web" href="#videoModal" data-target="#videoModal" data-toggle="modal">
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-lg-12 wow fadeIn">
                    <img src="img/svg/man_and_fire.svg" style="clear: both; width: 150px" />
                    <h1>Since The Beginning...</h1>
                    <span class="play-btn  pulse animated infinite">
                        <i class="fa fa-play"></i>
                    </span>
                </div>
            </div>
        </div>
    </section>

    <section id="weredifferent" style="padding-bottom: 0;" >
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-12 wow fadeIn">
                    <h1>Your perfect branding solution</h1>
                    <p class="col-xs-12 col-md-8 col-md-offset-2">
                        Content, hand-picked to capture your brand, delivered via unparalleled technology. It’s the perfect partnership between art and science.
                    </p>
                    <p class="col-xs-12  col-md-8 col-md-offset-2 ">
                        We’re not a pre-packaged, cookie cutter player or run-of-the-mill background music supplier. You’re partnering with our team of music consultants, commercial software developers, licensing experts and customer service guns who are with you every step of the way. 
                    </p>
            <img src="img/your_brand_record.png" class="col-xs-12 col-md-6 col-md-offset-3 img-responsive" />
                </div>
            </div>
        </div>
    </section>
    <section id="musicbrief" class="greylight">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-12 wow fadeIn">
                    <h1>Capture your brand's essence</h1>
                    <span class="col-xs-12 col-md-6 col-md-offset-3 ">
                        <p>Lay down on our couch and tell us about yourself... We go the extra mile to capture a thorough venue brief. We understand your brand, atmosphere, demographics and how it all changes across the day. </p>
                    </span>
                </div>
            </div>
        </div>
    </section>
    <section id="music">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-12 wow fadeIn">
                    <h1>Find your perfect sound</h1>
                    <p class="col-xs-12 col-md-6 col-md-offset-3">
                        It’s like your own in-house DJ who nails your brief every time. A soundtrack that is unique to you; hand-picked and remastered.
                    </p>
                </div>
            </div>
        </div>
    </section>  

    <span class="row-image cityscape"></span> 
    
    <section id="unleash" class="blue">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-12 wow fadeIn">
                    <h1>Unleash your vision</h1>
                    <p class="col-xs-12 col-md-6 col-md-offset-3">Use the best clips and our integrated Digital Signage platform to ensure that your customers see and hear exactly what you want them to, and nothing you don’t. </p>
                </div>
            </div>
        </div>
    </section> 

    <section id="managementinteraction">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 wow fadeIn">
                    <h1>Get interactive</h1>
                    <p>Desktops to mobiles, CEOs to customers – our range of apps let you manage and interact with everything, everywhere. </p>
                    <a href="<?php echo $what_we_do;?>#products"><button class="btn btn-success">See More <i class="fa fa-angle-right"></i></button></a>
                </div>
                <div class="col-lg-6 wow fadeIn">
                    <img src="img/products/manage_my_nightlife_devices.png" class="img-responsive" />
                </div>
            </div>
        </div>
    </section>
    <section id="ourservice" class="blue">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-12 wow fadeIn">
                    <h1>Scale to Grow</h1>
                        <p class="col-xs-12 col-md-6 col-md-offset-3">Whether you are managing 1 location or 1000 locations we make it easy with our Enterprise solution to keep your brand and messages consistent or help you tailor a unique identity for each location.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="music">
    <div class="container-fluid">
        <div class="row text-center content-row">
            <div class="row text-center icon-columns">
                <div class="col-md-2 col-md-offset-2 col-sm-6 wow fadeIn" data-wow-delay=".2s">
                    <div>
                        <i class="fa fa-clock-o fa-4x"></i>
                        <h3>Fully Scheduled</h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                    <div>
                        <i class="fa fa-gavel fa-4x"></i>
                        <h3>100% Legal </h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 wow fadeIn" data-wow-delay=".6s">
                    <div>
                        <i class="fa fa-medkit fa-4x"></i>
                        <h3>24/7 Support </h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 wow fadeIn" data-wow-delay=".8s">
                    <div>
                        <i class="fa fa-heart fa-4x"></i>
                        <h3>Reliable Hardware </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    <section id="clients">
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-xs-12">
                    <h1>Enjoyed by over 3 million Australians every month</h1>
                </div>
            </div>
        </div>
    </section>


<section class="standard-carousel clients-carousel wow fadeIn">
    <div class="item matisse">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="project-details">
                        <span class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                            <span class="col-xs-12">
                                <span class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                    <img src="img/client-logos/matisse-logo-gradient.svg" class="client-logo img-responsive" alt="">
                                </span>
                            </span>
                            <span class="project-description quote">"Thanks to Nightlife Music we have a custom curated playlist that adds the right amount of glamour and relaxation across all areas of the venue."</span>
                            <span class="project-description author">Gina Brand, Venue Manager - <a href="http://www.matissebeachclub.com.au" target="_blank">Matisse Beach Club <i class="fa fa-external-link"></i></a></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item snapfitness">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="project-details">
                        <span class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                            <span class="col-xs-12">
                                <span class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                    <img src="img/client-logos/logo_snap_fitness.png" class="client-logo img-responsive" alt="">
                                </span>
                            </span>
                            <span class="project-description quote">"Our members love the music, especially choosing songs with the kiosk! We see fewer headphones, enjoy more interaction and we still have total control!"</span>
                            <span class="project-description author">David Noonan, Managing Director - <a href="http://www.snapfitness.com.au/" target="_blank">Snap Fitness <i class="fa fa-external-link"></i></a></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item frankstonrsl">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="project-details">
                        <span class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                            <span class="col-xs-12">
                                <span class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
                                    <img src="img/client-logos/logo_frankston_rsl.png" class="client-logo img-responsive" alt="">
                                </span>
                            </span>
                            <span class="project-description quote">"Nightlife has made our lives easy. We manage our music, raffles, promotions and visuals from one spot and we use their mobile apps to do it all while on the go. "</span>
                            <span class="project-description author"><a href="http://www.frankstonrsl.com.au/" target="_blank">Frankston RSL <i class="fa fa-external-link"></i></a></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>        
    <div class="item collaroyhotel">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="project-details">
                        <span class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                            <span class="col-xs-12">
                                <span class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
                                    <img src="img/client-logos/logo_collaroy_hotel.png" class="client-logo img-responsive" alt="">
                                </span>
                            </span>
                            <span class="project-description quote">"Music was a critical to getting the positioning right with the Collaroy. Nightlife helped make sure that the experience is something that locals could relate to, but also a signal that they were somewhere pretty special."</span>
                            <span class="project-description author">Alistair Flower, General Manager - <a href="http://www.collaroyhotel.com.au/" target="_blank">Collaroy Hotel <i class="fa fa-external-link"></i></a></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item vivalavida">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="project-details">
                        <span class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                            <span class="col-xs-12">
                                <span class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                                    <img src="img/client-logos/logo_viva_la_vida.png" class="client-logo img-responsive" alt="">
                                </span>
                            </span>
                            <span class="project-description quote">"We needed someone who could find us the best music and keep it constantly fresh. Nightlife are the experts and they are always there to help us out!"</span>
                            <span class="project-description author">Kate Staples, Director - <a href="http://vivalavidadarwin.com.au/" target="_blank"> Viva La Vida Wine & Tapas Bar <i class="fa fa-external-link"></i></a></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>
    <section class="cta-form bg-dark">
        <div class="container text-center">
            <h3>Subscribe to our newsletter!</h3>
            <hr class="colored">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <!-- MailChimp Signup Form -->
                    <div id="mc_embed_signup">
                        
                        <!-- Replace the form action in the line below with your MailChimp embed action! For more informatin on how to do this please visit the Docs! -->
                        <form role="form" action="//nightlife.us6.list-manage.com/subscribe/post?u=5c66465b857415f4c1db07146&amp;id=f51c6bec32" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate>
                            <div class="input-group input-group-lg">
                                <input type="email" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="Email address...">
                                <span class="input-group-btn">
                                    <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary">Subscribe!</button>
                                </span>
                            </div>
                            <div id="mce-responses">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_5c66465b857415f4c1db07146_f51c6bec32" tabindex="-1" value=""></div>
                        </form>
                    </div>
                    <!-- End MailChimp Signup Form -->
                </div>
            </div>
        </div>
    </section>
    <section id="contact" style="background: #f2f2f2">
        <div class="container wow fadeIn">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Let's Bring <span class="underline">Your</span> Venue to Life!</h2>
                </div>
            </div>
            <div class="row content-row">
                <div class="col-lg-5">
                    <div class="panel">
                        <div class="panel-body">
                            <h3><i class="fa fa-phone"></i> Call Us</h3>
                            <p>Enquiries: 1800 679 748</p>
                            <p>Service: 1800 773 468</p>
                            

                            <h3><i class="fa fa-pencil"></i> Write</h3>
                            <p><a href="mailto:info@nightlife.com.au">info@nightlife.com.au</a></p>
                            <p><a href="http://www.nightlife.com.au">www.nightlife.com.au</a></p>
                        </div>
                    </div>

                    <h3>Learn More About...</h3>
                    <a href="<?php echo $what_we_do; ?>#music">
                        <button class="btn btn-outline-dark">Music</button>
                    </a>
                    <a href="<?php echo $what_we_do; ?>#licensing">
                        <button class="btn btn-outline-dark">Licensing</button>
                    </a>
                    <a href="<?php echo $what_we_do; ?>#visuals">
                        <button class="btn btn-outline-dark">Visuals</button>
                    </a>
                    <a href="<?php echo $what_we_do; ?>#digitalsignage">
                        <button class="btn btn-outline-dark">Digital Signage</button>
                    </a>
                    <a href="<?php echo $what_we_do; ?>#products">
                        <button class="btn btn-outline-dark">Products &amp; Features</button>
                    </a>
                    <a href="<?php echo $what_we_do; ?>#products">
                        <button class="btn btn-outline-dark">Hardware</button>
                    </a>
                    <a href="<?php echo $about_us; ?>">
                        <button class="btn btn-outline-dark">About Us</button>
                    </a>      
                  <a href="<?php echo $faq; ?>">
                        <button class="btn btn-outline-dark">FAQs</button>
                    </a>  
                </div>
                <div class="col-lg-7">
                    <?php include_once('pages/components/contact_form.php'); ?>
                </div>
            </div>
        </div>
    </section>

<!-- Home Video Modal -->
<div class="portfolio-modal modal fade video-modal" id="homeVideoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal"> 
            <button class="btn btn-default"><i class="fa fa-angle-left"></i> Back</button>
         </div>
        <div class="modal-body">
            <video id="genoverview_video" class="video-js vjs-default-skin fs-video" controls data-setup='{"autoplay": true, "preload": "auto" }'height="200" >
                <source src="mp4/NL_GeneralOverview_20150625_720p25_Web-Ready.mp4" type="video/mp4" />
                <source src="webm/NL_GeneralOverview_20150625_720p25_Web-Ready.webm" type="video/webm" />
                <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
            </video>
        </div>
    </div>
</div>
<!-- Infographic Video Modal -->
<div class="portfolio-modal modal fade video-modal" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal"> 
            <button class="btn btn-default"><i class="fa fa-angle-left"></i> Back</button>
         </div>
        <div class="modal-body">
            <video id="infographic_video" class="video-js vjs-default-skin fs-video" controls data-setup='{"autoplay": true, "preload": "auto" }'height="200" >
                <source src="mp4/NL_Infographic_20150616_720p25_Web-Ready.mp4" type="video/mp4" />
                <source src="mp4/NL_Infographic_20150616_720p25_Web-Ready.webm" type="video/webm" />
                <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
            </video>
        </div>
    </div>
</div>