<html>

<head>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="js/jquery.js"></script>
</head>

<body>
	<div class="col-xs-3"></div>
	<div class="col-xs-6 content">
		<h1>NM Website Builder Tester</h1>
		<div class="btn btn-success">Click to Build</div>
		<hr />
	</div>
	<div class="col-xs-3"></div>


	<script>
		$('.btn-success').on('click', function(){
			$.ajax({
				url: 'php/build_site.php?action=build'
			}).done(function( data ) {
				$('.content').append("<div class='alert alert-success' role='alert'>" + data + "<hr /><a href='index.html' target='_blank' class='btn btn-default'>Visit Site</a></div>");
			    if ( console && console.log ) {
			      console.log( "Sample of data:", data.slice( 0, 100 ) );
			    }
		  	});
		});
	</script>
</body>
</html>
