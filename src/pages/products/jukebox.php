<section id="mmn" class="row text-center"  style="background-image: url('img/what_we_do/products/nightlife_jukebox.jpg');color: white;">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-2">
		<div class="col-lg-8 col-lg-offset-2">
		    <i class="nm-icons-lm-juke fa-4x"></i>
		    <h2>Jukebox</h2>
		    <hr class="colored">
		</div>
		<div class="col-lg-8 col-lg-offset-2">
		    <p>Whether you are chasing some old school nostalgia or looking for a modern way to let your customers select what plays, we can help. </p>
		    <p>Our Jukeboxes have all the classic appeal of traditional jukes but with powerful music management under the hood. You can control what music is available when. </p>Plus they integrate seamlessly with your Nightlife System so that when a customer pays for a track it adds into your playlist and keeps your music playing smoothly. </p>
		    <p>On the Nightlife Jukebox, you can display advertisements, set the cost of a song and view detailed statistics on popular song selections and monitor revenue. If you are looking for a modern take on the Jukebox check out our Touch Panels and Kiosks.</p>
		</div>	
	</div>
</section>