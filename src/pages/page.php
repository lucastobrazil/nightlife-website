<!-- Instructions: Set your background image using the URL below. -->
    <header class="video">
        <div class="overlay"></div>
        <div class="intro-content">
            <!--<img src="img/profile.png" class="img-responsive img-centered" alt="">-->
            <div class="brand-name">Harness The Power of Music</div>
            <!--<hr class="colored">--> 
            <div class="brand-name-subtext">
                <span>It's your music, your vision and your identity. </span>
                <span>Delivered with full control, powerful automation</span>
                <span>and expert support.</span>
            </div>
        </div>
        <div class="homeslide-footer">
            <div class="scroll-down page-scroll">
                <a class="btn page-scroll" href="#youredifferent"><i class="fa fa-angle-down"></i></a>
            </div>
            <div class="social-bar">
                <div class="hidden-xs col-sm-3"></div>
                <div class="col-xs-12 col-sm-6">
                    <h2>Latest:</h2>
                    <?php include('php/tweets.php');?>
                </div>
                <div class="hidden-xs col-sm-3">
                    <a href="https://twitter.com/nightlifemusic" target="_blank">
                        <svg version="1.1" id="social_twitter" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve">
                            <path fill="#ffffff" d="M0,0v200.1h200.4V0.4L0,0z M165.4,69.6c0.1,1.3,0.1,2.6,0.1,4c0,40.5-30.8,87.2-87.2,87.2
                                c-17.3,0-33.4-5.1-47-13.8c2.4,0.3,4.8,0.4,7.3,0.4c14.4,0,27.6-4.9,38.1-13.1c-13.4-0.2-24.7-9.1-28.6-21.3
                                c1.9,0.4,3.8,0.5,5.8,0.5c2.8,0,5.5-0.4,8.1-1.1c-14-2.8-24.6-15.2-24.6-30.1c0-0.1,0-0.3,0-0.4c4.1,2.3,8.9,3.7,13.9,3.8
                                C42.9,80.4,37.5,71,37.5,60.4c0-5.6,1.5-10.9,4.2-15.4c15.1,18.5,37.7,30.7,63.2,32c-0.5-2.2-0.8-4.6-0.8-7
                                c0-16.9,13.7-30.7,30.7-30.7c8.8,0,16.8,3.7,22.4,9.7c7-1.4,13.5-3.9,19.5-7.4c-2.3,7.2-7.1,13.2-13.5,17c6.2-0.7,12.1-2.4,17.6-4.8
                                C176.6,59.9,171.4,65.3,165.4,69.6z"/>
                        </svg>
                    </a>
                    <a href="https://www.facebook.com/NightlifeMusicOfficial" target="_blank">
                        <svg version="1.1" id="social_facebook" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve">
                            <path fill="#ffffff" d="M0,0.7v199.8l200,0V0L0,0.7z M169.9,43.4l-15.9,0c-12.5,0-14.9,5.9-14.9,14.6v19.2h29.7l-3.9,30h-25.8v77
                                h-31v-77H82.2v-30h25.9V55c0-25.7,15.7-39.6,38.6-39.6c11,0,20.4,0.8,23.1,1.2V43.4z"/>
                        </svg>
                    </a>
                    <a href="https://instagram.com/nightlifemusic" target="_blank">
                        <svg version="1.1" id="social_instagram" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve">
                            <path fill-rule="evenodd" clip-rule="evenodd" fill="#ffffff" d="M178.5,84.5h-17.9c1.3,5.1,2.1,10.3,2.1,15.8
                                c0,34.9-28.3,63.2-63.1,63.2c-34.9,0-63.1-28.3-63.1-63.2c0-5.5,0.8-10.7,2.1-15.8H20.6v86.8c0,4.4,3.5,7.9,7.9,7.9h142.1
                                c4.4,0,7.9-3.5,7.9-7.9V84.5z M178.5,29.3c0-4.4-3.5-7.9-7.9-7.9h-23.7c-4.4,0-7.9,3.5-7.9,7.9v23.7c0,4.4,3.5,7.9,7.9,7.9h23.7
                                c4.4,0,7.9-3.5,7.9-7.9V29.3z M99.5,60.8c-21.8,0-39.5,17.7-39.5,39.5c0,21.8,17.7,39.5,39.5,39.5c21.8,0,39.5-17.7,39.5-39.5
                                C139,78.5,121.3,60.8,99.5,60.8 M200,199.9L0,199.8V0l200,0.1"/>
                        </svg>
                    </a>
                </div>
            </div>

            
        </div>
    </header>
    <section id="youredifferent" data-cat="web" href="#videoModal" data-toggle="modal" style="background-image:url('mp4/sample_poster.jpg'); height: 400px; background-position: 50%; text-align: center;">
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-lg-12 wow fadeIn">
                    <h1 style="color: white;">Since the beginning...</h1>
                    <span<i class="fa fa-play-circle-o" style="font-size: 35px; color: white; "></i>
                </div>
            </div>
        </div>
    </section>
    
    <section id="weredifferent">
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-lg-12 wow fadeIn">
                    <h1>We're different.</h1>
                    <p>Vitality is the perfect theme for a freelance professional or an agency.</p>
                    <hr class="colored">
                </div>
            </div>
            <div class="row text-center content-row">
                <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay=".2s">
                    <div class="about-content">
                        <i class="fa fa-eye fa-4x"></i>
                        <h3>Retina Ready</h3>
                        <p>This theme includes the retina.js plugin for easy retina image support so your images and graphics will look great on mobile devices!</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                    <div class="about-content">
                        <i class="fa fa-edit fa-4x"></i>
                        <h3>Easy to Edit</h3>
                        <p>Vitality is built using Bootstrap 3, and is easy to edit and customize. LESS files are included as well for deeper customization.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay=".6s">
                    <div class="about-content">
                        <i class="fa fa-tablet fa-4x"></i>
                        <h3>Responsive</h3>
                        <p>When building this theme, we've paid close attention to how it looks on various screen sizes. Try it out on a phone or tablet!</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay=".8s">
                    <div class="about-content">
                        <i class="fa fa-heart fa-4x"></i>
                        <h3>Built with Love</h3>
                        <p>All of our themes are crafted with care, and we strive to make your experience as one of our customers the best as possible.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-gray" id="weredifferent">
        <div class="container text-center wow fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Our Team</h2>
                    <p>We are a group of digital marketers with a passion for great art that serves a practical purpose.</p>
                    <hr class="colored">
                </div>
            </div>
            <div class="row content-row">
                <div class="col-lg-12">
                    <div class="about-carousel">
                        <div class="item">
                            <img src="img/people/1.jpg" class="img-responsive" alt="">
                            <div class="caption">
                                <h3>Patricia West</h3>
                                <hr class="colored">
                                <p>Marketing Director</p>
                                <ul class="list-inline social">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook fa-fw"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter fa-fw"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-linkedin fa-fw"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <img src="img/people/2.jpg" class="img-responsive" alt="">
                            <div class="caption">
                                <h3>Howard Scott</h3>
                                <hr class="colored">
                                <p>Sales Manager</p>
                                <ul class="list-inline social">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook fa-fw"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter fa-fw"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-linkedin fa-fw"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <img src="img/people/3.jpg" class="img-responsive" alt="">
                            <div class="caption">
                                <h3>Kate Williams</h3>
                                <hr class="colored">
                                <p>Creative Director</p>
                                <ul class="list-inline social">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook fa-fw"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter fa-fw"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-linkedin fa-fw"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-dribbble fa-fw"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <img src="img/people/4.jpg" class="img-responsive" alt="">
                            <div class="caption">
                                <h3>Amy Vanderbute</h3>
                                <hr class="colored">
                                <p>Graphic Artist</p>
                                <ul class="list-inline social">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook fa-fw"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter fa-fw"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-linkedin fa-fw"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-dribbble fa-fw"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <img src="img/people/5.jpg" class="img-responsive" alt="">
                            <div class="caption">
                                <h3>Jeremy Davidson</h3>
                                <hr class="colored">
                                <p>Web Developer</p>
                                <ul class="list-inline social">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook fa-fw"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter fa-fw"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-linkedin fa-fw"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-github fa-fw"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <aside class="cta-quote" style="background-image: url('img/bg-sky.jpg');">
        <div class="container wow fadeIn">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <span class="quote">Good <span class="text-primary">design</span> is finding that perfect balance between the way something <span class="text-primary">looks</span> and how it <span class="text-primary">functions</span>.</span>
                    <hr class=" colored">
                    <a class="btn btn-outline-light page-scroll" href="#contact">Let's Find It</a>
                </div>
            </div>
        </div>
    </aside>
    <section id="designing" class="services" style="background-image: url('img/bg-sky.jpg');">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-12 wow fadeIn">
                    <h1>Designing Your Solution</h1>
                    <hr class="colored">
                    <p>Nightlife is unique. We are a full service provider that delivers true scalability and value. The difference is in our people with over 90 dedicated staff focused on your business.</p>
                    <h2>Here’s what makes us different:</h2>
                </div>
            </div>
            <div class="row content-row">
                <div class="col-md-4 wow fadeIn" data-wow-delay=".2s">
                    <div class="media">
                        <div class="pull-left">
                            <i class="fa fa-clipboard"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Plan</h3>
                            <ul>
                                <li>Client interview</li>
                                <li>Gather consumer data</li>
                                <li>Create content strategy</li>
                                <li>Analyze research</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 wow fadeIn" data-wow-delay=".4s">
                    <div class="media">
                        <div class="pull-left">
                            <i class="fa fa-pencil"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Create</h3>
                            <ul>
                                <li>Build wireframe</li>
                                <li>Gather client feedback</li>
                                <li>Code development</li>
                                <li>Marketing review</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 wow fadeIn" data-wow-delay=".6s">
                    <div class="media">
                        <div class="pull-left">
                            <i class="fa fa-rocket"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Launch</h3>
                            <ul>
                                <li>Deploy website</li>
                                <li>Market product launch</li>
                                <li>Collect UX data</li>
                                <li>Quarterly maintenence</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="weknow" class="bg-gray">
        <div class="container text-center wow fadeIn">
            <h2>Our Work</h2>
            <hr class="colored">
            <p>Examples of our work which represent our design and marketing capabilities.</p>
        </div>
    </section>
    <section class="portfolio-carousel wow fadeIn">
        <div class="item" style="background-image: url('img/portfolio/bg-1.jpg')">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-md-push-8">
                        <div class="project-details">
                            <img src="img/client-logos/logo-1.png" class="img-responsive client-logo" alt="">
                            <span class="project-name">Project Name</span>
                            <span class="project-description">Branding, Website Design</span>
                            <hr class="colored">
                            <a href="#portfolioModal1" data-toggle="modal" class="btn btn-outline-light">View Details <i class="fa fa-long-arrow-right fa-fw"></i></a>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-pull-4 hidden-xs">
                        <img src="img/portfolio/mobile-screens.png" class="img-responsive portfolio-image" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="item" style="background-image: url('img/portfolio/bg-2.jpg')">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-md-push-8">
                        <div class="project-details">
                            <img src="img/client-logos/logo-2.png" class="img-responsive client-logo" alt="">
                            <span class="project-name">Project Name</span>
                            <span class="project-description">Branding, Website Design</span>
                            <hr class="colored">
                            <a href="#portfolioModal2" data-toggle="modal" class="btn btn-outline-light">View Details <i class="fa fa-long-arrow-right fa-fw"></i></a>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-pull-4 hidden-xs">
                        <img src="img/portfolio/tablet-screens.png" class="img-responsive portfolio-image" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="item" style="background-image: url('img/portfolio/bg-3.jpg')">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-md-push-8">
                        <div class="project-details">
                            <img src="img/client-logos/logo-1.png" class="img-responsive client-logo" alt="">
                            <span class="project-name">Project Name</span>
                            <span class="project-description">Branding, Website Design</span>
                            <hr class="colored">
                            <a href="#portfolioModal3" data-toggle="modal" class="btn btn-outline-light">View Details <i class="fa fa-long-arrow-right fa-fw"></i></a>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-pull-4 hidden-xs">
                        <img src="img/portfolio/mobile-screens.png" class="img-responsive portfolio-image" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="item" style="background-image: url('img/portfolio/bg-4.jpg')">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-md-push-8">
                        <div class="project-details">
                            <img src="img/client-logos/logo-2.png" class="img-responsive client-logo" alt="">
                            <span class="project-name">Project Name</span>
                            <span class="project-description">Branding, Website Design</span>
                            <hr class="colored">
                            <a href="#portfolioModal4" data-toggle="modal" class="btn btn-outline-light">View Details <i class="fa fa-long-arrow-right fa-fw"></i></a>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-pull-4 hidden-xs">
                        <img src="img/portfolio/tablet-screens.png" class="img-responsive portfolio-image" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container text-center wow fadeIn">
            <h2>Portfolio</h2>
            <hr class="colored">
            <p>Here are some other projects that I've worked on.</p>
            <div class="row content-row">
                <div class="col-lg-12">
                    <div class="portfolio-filter">
                        <ul id="filters" class="clearfix">
                            <li>
                                <span class="filter active" data-filter="identity graphic logo web">All</span>
                            </li>
                            <li>
                                <span class="filter" data-filter="identity">Identity</span>
                            </li>
                            <li>
                                <span class="filter" data-filter="graphic">Graphic</span>
                            </li>
                            <li>
                                <span class="filter" data-filter="web">Web</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div id="portfoliolist">
                        <div class="portfolio identity" data-cat="identity" href="#portfolioModal1" data-toggle="modal">
                            <div class="portfolio-wrapper">
                                <img src="img/portfolio/grid/identity/1.jpg" alt="">
                                <div class="caption">
                                    <div class="caption-text">
                                        <a class="text-title">Generic Inc.</a>
                                        <span class="text-category">Brand Identity</span>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio web" data-cat="web" href="#portfolioModal2" data-toggle="modal">
                            <div class="portfolio-wrapper">
                                <img src="img/portfolio/grid/web/1.jpg" alt="">
                                <div class="caption">
                                    <div class="caption-text">
                                        <a class="text-title">Brands</a>
                                        <span class="text-category">Web Development</span>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio graphic" data-cat="graphic" href="#portfolioModal3" data-toggle="modal">
                            <div class="portfolio-wrapper">
                                <img src="img/portfolio/grid/graphic/1.jpg" alt="">
                                <div class="caption">
                                    <div class="caption-text">
                                        <a class="text-title">Excellence</a>
                                        <span class="text-category">Poster Design</span>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio web" data-cat="web" href="#portfolioModal4" data-toggle="modal">
                            <div class="portfolio-wrapper">
                                <img src="img/portfolio/grid/web/2.jpg" alt="">
                                <div class="caption">
                                    <div class="caption-text">
                                        <a class="text-title">YouTV</a>
                                        <span class="text-category">Web Development</span>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio graphic" data-cat="graphic" href="#portfolioModal1" data-toggle="modal">
                            <div class="portfolio-wrapper">
                                <img src="img/portfolio/grid/graphic/2.jpg" alt="">
                                <div class="caption">
                                    <div class="caption-text">
                                        <a class="text-title">Office Supplies</a>
                                        <span class="text-category">Graphic Design</span>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio web" data-cat="web" href="#portfolioModal2" data-toggle="modal">
                            <div class="portfolio-wrapper">
                                <img src="img/portfolio/grid/web/3.jpg" alt="">
                                <div class="caption">
                                    <div class="caption-text">
                                        <a class="text-title">Fooder</a>
                                        <span class="text-category">Mobile App</span>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio graphic" data-cat="graphic" href="#portfolioModal3" data-toggle="modal">
                            <div class="portfolio-wrapper">
                                <img src="img/portfolio/grid/graphic/3.jpg" alt="">
                                <div class="caption">
                                    <div class="caption-text">
                                        <a class="text-title">Mountain Tea</a>
                                        <span class="text-category">Graphic Design</span>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio graphic" data-cat="graphic" href="#portfolioModal4" data-toggle="modal">
                            <div class="portfolio-wrapper">
                                <img src="img/portfolio/grid/graphic/4.jpg" alt="">
                                <div class="caption">
                                    <div class="caption-text">
                                        <a class="text-title">Checkmark</a>
                                        <span class="text-category">Graphic Design</span>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio identity" data-cat="identity" href="#portfolioModal1" data-toggle="modal">
                            <div class="portfolio-wrapper">
                                <img src="img/portfolio/grid/identity/2.jpg" alt="">
                                <div class="caption">
                                    <div class="caption-text">
                                        <a class="text-title">Demish</a>
                                        <span class="text-category">Brand Identity</span>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio web" data-cat="web" href="#portfolioModal2" data-toggle="modal">
                            <div class="portfolio-wrapper">
                                <img src="img/portfolio/grid/web/4.jpg" alt="">
                                <div class="caption">
                                    <div class="caption-text">
                                        <a class="text-title">OneRepublic</a>
                                        <span class="text-category">Web Development</span>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio graphic" data-cat="graphic" href="#portfolioModal3" data-toggle="modal">
                            <div class="portfolio-wrapper">
                                <img src="img/portfolio/grid/graphic/5.jpg" alt="">
                                <div class="caption">
                                    <div class="caption-text">
                                        <a class="text-title">Postable</a>
                                        <span class="text-category">Graphic Design</span>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio identity" data-cat="identity" href="#portfolioModal4" data-toggle="modal">
                            <div class="portfolio-wrapper">
                                <img src="img/portfolio/grid/identity/3.jpg" alt="">
                                <div class="caption">
                                    <div class="caption-text">
                                        <a class="text-title">Simpli Nota</a>
                                        <span class="text-category">Brand Identity</span>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonials bg-gray">
        <div class="container wow fadeIn">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="testimonials-carousel">
                        <div class="item">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p class="lead">"Working with Vitality was both a valuable and rewarding experience."</p>
                                    <hr class="colored">
                                    <p class="quote">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit, temporibus, laborum, dignissimos doloremque corporis alias nostrum recusandae culpa id quisquam harum impedit sed sunt non obcaecati vero ipsam aut fugit?</p>
                                    <div class="testimonial-info">
                                        <div class="testimonial-img">
                                            <img src="img/testimonials/1.jpg" class="img-circle img-responsive" alt="">
                                        </div>
                                        <div class="testimonial-author">
                                            <span class="name">Jim Walker</span>
                                            <p class="small">CEO of GeneriCorp</p>
                                            <div class="stars">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p class="lead">"Working with Vitality was both a valuable and rewarding experience."</p>
                                    <hr class="colored">
                                    <p class="quote">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit, temporibus, laborum, dignissimos doloremque corporis alias nostrum recusandae culpa id quisquam harum impedit sed sunt non obcaecati vero ipsam aut fugit?</p>
                                    <div class="testimonial-info">
                                        <div class="testimonial-img">
                                            <img src="img/testimonials/1.jpg" class="img-circle img-responsive" alt="">
                                        </div>
                                        <div class="testimonial-author">
                                            <span class="name">Jim Walker</span>
                                            <p class="small">CEO of GeneriCorp</p>
                                            <div class="stars">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="pricing" class="pricing" style="background-image: url('img/bg-pricing.jpg')">
        <div class="container wow fadeIn">
            <div class="row text-center">
                <div class="col-lg-12">
                    <h2>Pricing</h2>
                    <hr class="colored">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum, quae, laborum, voluptate delectus odio doloremque error porro obcaecati nemo animi ducimus quaerat nostrum? Ab molestiae eaque et atque architecto reiciendis.</p>
                </div>
            </div>
            <div class="row content-row">
                <div class="col-md-4">
                    <div class="pricing-item featured-first">
                        <h3>Basic</h3>
                        <hr class="colored">
                        <div class="price"><span class="number"><sup>$</sup>25</span> / month</div>
                        <ul class="list-group">
                            <li class="list-group-item">60 Users</li>
                            <li class="list-group-item">Unlimited Forums</li>
                            <li class="list-group-item">Unlimited Reports</li>
                            <li class="list-group-item">3,000 Entries per Month</li>
                            <li class="list-group-item">200 MB Storage</li>
                            <li class="list-group-item">Unlimited Support</li>
                        </ul>
                        <a href="#" class="btn btn-outline-dark">Sign Up</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="pricing-item featured">
                        <h3>Plus</h3>
                        <hr class="colored">
                        <div class="price"><span class="number"><sup>$</sup>50</span> / month</div>
                        <ul class="list-group">
                            <li class="list-group-item"><strong>60</strong> Users</li>
                            <li class="list-group-item">Unlimited Forums</li>
                            <li class="list-group-item">Unlimited Reports</li>
                            <li class="list-group-item"><strong>3,000</strong> Entries per Month</li>
                            <li class="list-group-item"><strong>200 MB</strong> Storage</li>
                            <li class="list-group-item">Unlimited Support</li>
                        </ul>
                        <a href="#" class="btn btn-outline-dark">Sign Up</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="pricing-item featured-last">
                        <h3>Premium</h3>
                        <hr class="colored">
                        <div class="price"><span class="number"><sup>$</sup>150</span> / month</div>
                        <ul class="list-group">
                            <li class="list-group-item">60 Users</li>
                            <li class="list-group-item">Unlimited Forums</li>
                            <li class="list-group-item">Unlimited Reports</li>
                            <li class="list-group-item">3,000 Entries per Month</li>
                            <li class="list-group-item">200 MB Storage</li>
                            <li class="list-group-item">Unlimited Support</li>
                        </ul>
                        <a href="#" class="btn btn-outline-dark">Sign Up</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cta-form bg-dark">
        <div class="container text-center">
            <h3>Subscribe to our newsletter!</h3>
            <hr class="colored">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <!-- MailChimp Signup Form -->
                    <div id="mc_embed_signup">
                        <!-- Replace the form action in the line below with your MailChimp embed action! For more informatin on how to do this please visit the Docs! -->
                        <form role="form" action="//startbootstrap.us3.list-manage.com/subscribe/post?u=531af730d8629808bd96cf489&amp;id=afb284632f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate>
                            <div class="input-group input-group-lg">
                                <input type="email" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="Email address...">
                                <span class="input-group-btn">
                                    <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary">Subscribe!</button>
                                </span>
                            </div>
                            <div id="mce-responses">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>
                        </form>
                    </div>
                    <!-- End MailChimp Signup Form -->
                </div>
            </div>
        </div>
    </section>
    <section id="contact">
        <div class="container wow fadeIn">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Contact Us</h2>
                    <hr class="colored">
                    <p>Please tell us about your next project and we will let you know what we can do to help you.</p>
                </div>
            </div>
            <div class="row content-row">
                <div class="col-lg-8 col-lg-offset-2">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Name" id="name" required data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Email Address</label>
                                <input type="email" class="form-control" placeholder="Email Address" id="email" required data-validation-required-message="Please enter your email address.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Phone Number</label>
                                <input type="tel" class="form-control" placeholder="Phone Number" id="phone" required data-validation-required-message="Please enter your phone number.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Message</label>
                                <textarea rows="5" class="form-control" placeholder="Message" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <br>
                        <div id="success"></div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <button type="submit" class="btn btn-outline-dark">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <a href="https://wrapbootstrap.com/theme/vitality-multipurpose-one-page-theme-WB02K3KK3" class="btn btn-block btn-full-width">Buy Vitality Now!</a>
    <footer class="footer" style="background-image: url('img/bg-footer.jpg')">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-4 contact-details">
                    <h4><i class="fa fa-phone"></i> Call</h4>
                    <p>555-213-4567</p>
                </div>
                <div class="col-md-4 contact-details">
                    <h4><i class="fa fa-map-marker"></i> Visit</h4>
                    <p>3481 Melrose Place
                        <br>Beverly Hills, CA 90210</p>
                </div>
                <div class="col-md-4 contact-details">
                    <h4><i class="fa fa-envelope"></i> Email</h4>
                    <p><a href="mailto:mail@example.com">mail@example.com</a>
                    </p>
                </div>
            </div>
            <div class="row social">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li><a href="#"><i class="fa fa-facebook fa-fw fa-2x"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-twitter fa-fw fa-2x"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin fa-fw fa-2x"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row copyright">
                <div class="col-lg-12">
                    <p class="small">&copy; 2015 Start Bootstrap Themes</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Portfolio Modals -->
    <!-- Example Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true" style="background-image: url('img/portfolio/bg-1.jpg')">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <img src="img/client-logos/logo-1.png" class="img-responsive img-centered" alt="">
                            <h2>Project Title</h2>
                            <hr class="colored">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos modi in tenetur vero voluptatum sapiente dolores eligendi nemo iste ea. Omnis, odio enim sint quam dolorum dolorem. Nostrum, minus, ad.</p>
                        </div>
                        <div class="col-lg-12">
                            <img src="img/portfolio/mobile-screens.png" class="img-responsive img-centered" alt="">
                        </div>
                        <div class="col-lg-8 col-lg-offset-2">
                            <ul class="list-inline item-details">
                                <li>Client: <strong><a href="http://startbootstrap.com">Start Bootstrap</a></strong>
                                </li>
                                <li>Date: <strong><a href="http://startbootstrap.com">April 2015</a></strong>
                                </li>
                                <li>Service: <strong><a href="http://startbootstrap.com">Web Development</a></strong>
                                </li>
                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Example Modal 2 -->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true" style="background-image: url('img/portfolio/bg-2.jpg')">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <img src="img/client-logos/logo-2.png" class="img-responsive img-centered" alt="">
                            <h2>Project Title</h2>
                            <hr class="colored">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos modi in tenetur vero voluptatum sapiente dolores eligendi nemo iste ea. Omnis, odio enim sint quam dolorum dolorem. Nostrum, minus, ad.</p>
                        </div>
                        <div class="col-lg-12">
                            <img src="img/portfolio/tablet-screens.png" class="img-responsive img-centered" alt="">
                        </div>
                        <div class="col-lg-8 col-lg-offset-2">
                            <ul class="list-inline item-details">
                                <li>Client: <strong><a href="http://startbootstrap.com">Start Bootstrap</a></strong>
                                </li>
                                <li>Date: <strong><a href="http://startbootstrap.com">April 2015</a></strong>
                                </li>
                                <li>Service: <strong><a href="http://startbootstrap.com">Web Development</a></strong>
                                </li>
                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Example Modal 3 -->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true" style="background-image: url('img/portfolio/bg-3.jpg')">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <img src="img/client-logos/logo-1.png" class="img-responsive img-centered" alt="">
                            <h2>Project Title</h2>
                            <hr class="colored">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos modi in tenetur vero voluptatum sapiente dolores eligendi nemo iste ea. Omnis, odio enim sint quam dolorum dolorem. Nostrum, minus, ad.</p>
                        </div>
                        <div class="col-lg-12">
                            <img src="img/portfolio/mobile-screens.png" class="img-responsive img-centered" alt="">
                        </div>
                        <div class="col-lg-8 col-lg-offset-2">
                            <ul class="list-inline item-details">
                                <li>Client: <strong><a href="http://startbootstrap.com">Start Bootstrap</a></strong>
                                </li>
                                <li>Date: <strong><a href="http://startbootstrap.com">April 2015</a></strong>
                                </li>
                                <li>Service: <strong><a href="http://startbootstrap.com">Web Development</a></strong>
                                </li>
                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Example Modal 4 -->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true" style="background-image: url('img/portfolio/bg-4.jpg')">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <img src="img/client-logos/logo-2.png" class="img-responsive img-centered" alt="">
                            <h2>Project Title</h2>
                            <hr class="colored">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos modi in tenetur vero voluptatum sapiente dolores eligendi nemo iste ea. Omnis, odio enim sint quam dolorum dolorem. Nostrum, minus, ad.</p>
                        </div>
                        <div class="col-lg-12">
                            <img src="img/portfolio/tablet-screens.png" class="img-responsive img-centered" alt="">
                        </div>
                        <div class="col-lg-8 col-lg-offset-2">
                            <ul class="list-inline item-details">
                                <li>Client: <strong><a href="http://startbootstrap.com">Start Bootstrap</a></strong>
                                </li>
                                <li>Date: <strong><a href="http://startbootstrap.com">April 2015</a></strong>
                                </li>
                                <li>Service: <strong><a href="http://startbootstrap.com">Web Development</a></strong>
                                </li>
                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Video Modal -->
    <div class="portfolio-modal modal fade" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true" style="background-image: url('mp4/sample_poster.jpg')">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <video id="infographic_video" class="video-js vjs-default-skin" data-setup='{ "controls": true, "autoplay": true, "preload": "auto" }' poster="mp4/sample_poster.jpg" height="200" >
                    <source src="mp4/camera.mp4" type="video/mp4" />
                    <source src="webm/camera.webm" type="video/webm" />
                    <source src="http://video-js.zencoder.com/oceans-clip.ogv" type="video/ogg" />
                    <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                </video>
            </div>
        </div>
    </div>