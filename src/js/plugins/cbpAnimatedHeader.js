/**
 * cbpAnimatedHeader.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Codrops
 * http://www.codrops.com
 */
var cbpAnimatedHeader = (function() {

	var docElem = document.documentElement,
		header = document.querySelector( '.navbar-fixed-top' ),
		journeyNav = document.querySelector( '.journey-nav' ),
		navbarBrand = document.querySelector( '.navbar-brand' ),
		didScroll = false,
		changeHeaderOn = 200;

	function init() {
		// Lucas A - added so that if you refresh the page
		// Halfway down, the menu retracts. Instead of 
		// listening out for scroll only.
		$(document).ready(function(){
			if($(window).scrollTop() > changeHeaderOn){
				setTimeout( scrollPage, 5 );
			}
		});
		window.addEventListener( 'scroll', function( event ) {
			if( !didScroll ) {
				didScroll = true;
				setTimeout( scrollPage, 0 );
			}
		}, false );
	}

	function scrollPage() {
		var sy = scrollY();
		if ( sy >= changeHeaderOn ) {
			classie.remove( header, 'navbar-expanded');
			//if(showJourneyNav)
				//classie.remove( journeyNav, 'invisible');
				
			classie.remove(navbarBrand, 'white');

			//jQuery('.navbar-brand').find('path').css({fill: 'red'});
		}
		else {
			classie.add( header, 'navbar-expanded');
			//if(showJourneyNav)
				//classie.add( journeyNav, 'invisible');

			classie.add(navbarBrand, 'white');
		}
		didScroll = false;
	}

	function scrollY() {
		return window.pageYOffset || docElem.scrollTop;
	}

	init();

})();