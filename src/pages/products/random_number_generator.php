<section id="mmn" class="row text-center" style="background-image: url('img/what_we_do/products/random_number_generator.jpg'); color: white;">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-2">
		<div class="col-lg-8 col-lg-offset-2">
            <i class="fa fa-gift fa-4x"></i>
            <h2>Random Number Generator</h2>
            <hr class="colored">
        </div>
        <div class="col-lg-8 col-lg-offset-2">
            <p>Gone are the days of stealing someone's sweaty hat to draw out your raffle tickets. With our inbuilt random number generator you can conduct your own random draws live on screen and trigger it right from your mobile phone with Manage My Nightlife.</p>
        </div>
	</div>
</section>