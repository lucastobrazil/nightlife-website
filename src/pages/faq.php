
<section id="faq">
  <div class="container"  id="overview">
        <h1 class="col-xs-12">Frequently Asked Questions (FAQ)</h1>
    <div class="hidden-xs col-sm-3 pull-right">
        <nav id="faq-nav" class="affix">
            <p>Quick Jump</p>
            <ul class="nav " role="tablist">
                <li><a href="#overview" class="faq-page-scroll">Overview</a></li>
                <li><a href="#business" class="faq-page-scroll">Business</a></li>
                <li><a href="#installation" class="faq-page-scroll">Installation and Hardware</a></li>
                <li><a href="#service" class="faq-page-scroll">Service and Warranty</a></li>
                <li><a href="#content" class="faq-page-scroll">Music Content</a></li>
                <li><a href="#settings" class="faq-page-scroll">Music Settings</a></li>
                <li><a href="#visuals-da" class="faq-page-scroll">Visuals and Digital Advertising</a></li>
                <li><a href="#products" class="faq-page-scroll">Products</a></li>
                <li><a href="#licensing" class="faq-page-scroll">Public Performance Licensing</a></li>
            </ul>
        </nav>
    </div>
    <div class="col-xs-12 col-sm-9">
        <h2>Overview</h2>
        <h4>What is Nightlife Music?</h4>
          <p>Nightlife Music is Australia's premium commercial entertainment, content and advertising solution. We consult with venues across Australia to help create a unique sound and vision for their brand. </p>
        <h4>What kind of support does Nightlife Music provide?</h4> 
          <p>Our team of over 90+ staff are with you every step of the way. From the initial briefing to our ongoing training and music management we are here for you 24/7 and we want to help. Our Customer Relationship Management (CRM) system ensures that your vision is documented and that every Nightlife Music employee can quickly see the history of your account so we can help in real time with real solutions. </p>
        <h4>Why is Nightlife Music more cost effective than a digital music service?</h4>
          <p>The service that Nightlife Music provides goes far beyond a playlist and a player. We are music consultants who help translate your brand into something that your customers can hear and feel. With a digital music service you are left to choose the music and to get it right it takes skill and lots of time. The technology that we provide is cutting-edge but it is our human curation and service that makes the real difference in venues. </p>
        <h4>Is Nightlife Music available outside of Australia?</h4>
         <p>We are currently based within Australia but we are able to work with clients on special projects to help deliver the brand consistency they need into multiple markets. If you are looking for an international solution for your business have a talk to our team to explore your options. </p>
        <h4>How does Nightlife Music play in my venue?</h4>
        <p>We create a bespoke music and visual identity for you and deliver it on proprietary built hardware. To put it simply we give you all your music and visuals, on a powerful, commercial grade computer. The hardware is built by us to withstand rigorous use and it becomes the platform for our service to you. It plugs in to your existing AV network to deliver the music and visuals. You can build on this platform with any additional products you need like touch panels or Jukeboxes and this hardware also communicates with our management technology apps that you might choose to use. </p>
        <h4>What other products does Nightlife Music offer?</h4>
          <p>Through our in-house tech team, we are leading the way in meaningful venue management through technology. Every development we make builds seamlessly onto our existing systems. Our current products and services are: 
            Nightlife Music System
            Ambient Visuals 
            Digital Advertising
            Jukeboxes and Touch Panels
            Mobile apps including: Manage My Nightlife and crowdDJ 
            Karaoke
            Acoustic Consultancy
            Music Licensing 
          </p>
        <h2 id="business">Business</h2>
        <h4>How can I get Nightlife Music for my business?</h4>
        <p>It's easy! Just contact us so we can design a solution to suit your needs. Give us a call on 1800 679 748 or email info@nightlife.com.au </p>
        <h4>How much does Nightlife Music cost?</h4>
        <p>Nightlife Music is a subscription based service and your individual cost will depend on a range of factors like size, number of zones and the features that you need. We work to try and maximise your music and minimise your cost. To find out more give us a call at 1800 679 748, email info@nightlife.com.au or use the form on the contact us page and we will create a no obligation quote for your venue. </p>
        <h4>What type of businesses use Nightlife Music?</h4>
        <p>Any business that values their customers’ experience is the perfect fit for Nightlife Music. We currently have over 2700 clients across different industries such as hotels and pubs, bars and restaurants, casinos, cruise ships, family entertainment and leisure, gyms and retail. </p>
        <h4>How does Nightlife Music help with branding?</h4>
        <p>Everything that your customers experience when they interact with your business becomes a touch point for your brand. Music and visuals in your venue form a huge part of your customers’ experience. Nightlife Music helps you to translate your brand into sound and vision and deliver that with perfect quality and consistency. </p>
        <h4>Do I have control over the music played in my venue?</h4>
        <p>Completely! We take a full brief from you and hand-pick the music that suits your venue. On an ongoing basis we work with you to keep your music exactly as you want it. Plus you can be as hands on or off as you like with your music. Our suite of management tools make it easy to stay in control of what’s playing. </p>
        <h4>Does Nightlife Music have an API?</h4>
        <p>If you are interested in working with Nightlife Music and our technology talk to us! Existing clients can talk to their Nightlife team and other third parties should call us on 1800 679 748. </p>
        <h4>I am an AV agent and I love Nightlife, how can I work with you?</h4>
        <p>Great! We’d love to partner with you! Our AV network helps us deliver the best service in the industry and we are always looking for new partners. Give us a call on 1800 679 748 </p>
        
        <h2 id="installation">Installation and Hardware</h2>
        <h4>Can I use Nightlife Music from multiple locations?</h4>
        <p>Your Nightlife System can be configured to service as many different zones as you need across your venue. You can then manage your system remotely from anywhere using the web and mobile apps. </p>
        <h4>What hardware do I need to run Nightlife Music in my business?</h4>
        <p>Nightlife Music is intended for commercial use and our proprietary hardware is market-proven to ensure that you have a solution that mirrors the quality of your business. Depending on your needs we will have the hardware that you need. Management tools can then be integrated into your existing hardware including your desktop computers, POS systems, smartphones and tablets. </p>
        <h4>Can I just get your music and use my own gear? </h4>
        <p>Our proprietary hardware and software is the reason that we can offer our unique service. It lets us deliver content securely with full encryptions and because of this over 900 international music labels trust us with special access to their libraries of music; allowing us to remaster and deliver you the quality product that we are renowned for. </p>
        <h4>Do I need an internet connection?</h4>
        <p>Your Nightlife System will play without being online but for technical support as well as to take advantage of all the remote management tools your Nightlife System will require an internet connection. </p>
        <h4>Can I install the Nightlife System myself?</h4>
        <p>Yes, your Nightlife System is sent to you already loaded with your businesses unique profile. It is easy to set up and we can walk you through it. If you would rather have someone else install your system we can help to coordinate with your AV team or using our Australia-wide network of AV agents. </p>
        <h4>Will the system work with my high definition network?</h4>
        <p>Yes, we are fully compatible with higher definition networks. If you have particular requirements make sure you talk to us and we can find a solution that will suit you. </p>
        
        <h2 id="service">Service and Warranty</h2>
        <h4>Do you provide training?</h4>
        <p>Yes, from the initial set up and commissioning to new staff and job changeovers we are always available to train your team on the new and existing features. Our dedicated customer service team are here to help you get the most from your system. </p>
        <h4>What if something goes wrong?</h4>
        <p>Your Nightlife System is manufactured at our headquarters in Brisbane, Australia and is proprietary built to withstand the rigour of commercial use around the clock. In the unlikely event that something does go wrong however it is good to know that most faults can be diagnosed and fixed over the phone and remotely when your system is online. We also have an Australia-wide network of AV agents who we can call on to support you. Our technical team are available 24/7 for emergency support on the toll-free number 1800 679 748. </p>
        <h4>Is the system covered by Warranty?</h4>
        <p>Yes, all of our systems are back by an industry-leading warranty. You are covered by an initial 12 month free exchange warranty on all components and then we continue to protect your investment for the life of your subscription with a <a href="datastore/pdf/NightlifeMusic_WarrantyServicePolicy_09062015.pdf" target="_blank">fixed price exchange policy</a>. </p>
       
        <h2 id="content">Music Content</h2>
        <h4>What type of music is on the Nightlife Music system?</h4>
        <p>In short, whatever you need! We don’t supply a system with a generic library. We design a music profile just for your venue and find music that suits you. For our current clients we cover everything from Creole and Polka to the latest hits. Whether it’s light café, deep club beats or alternative fringe tracks we will help you discover the perfect sound. </p>
        <h4>How many songs do I get with Nightlife?</h4>
        <p>You have access to our complete library which is constantly updated and refined. Your personal selection is honed to ensure you can quickly find more of the music you want. We sort through the world’s music, over 40 million songs, and take the best from every genre resulting in a library that currently sits at over 40,000. This is constantly added to, ensuring that we always have the best music for your venue. </p>
        <h4>Why can't I find a particular song/artist in the library?</h4>
        <p>With over 40,000 songs available and an ever changing library there is a good chance we’ll have whatever you need. Some clients request different filters and user settings that restrict access to certain music styles to ensure that their brand identity is protected. If you are looking for a particular track just call and if we don’t already have it we will find it for you! </p>
        <h4>How does Nightlife Music curate the content?</h4>
        <p>We have team of music research experts who are constantly searching, reviewing and QAing new music. The team looks at the merit of each and every song and where it would fit into our library. They then have to prove its worth to the rest of the team. If a song makes it through this rigorous process it is then remastered for perfect playback quality and is added to the library. Our music consultants then work with our clients to curate the perfect soundtrack for them. </p>
        <h4>Do the songs play smoothly?</h4>
        <p>Yes, every song is mixed into the next – like you had a DJ on staff! That’s the simple answer; the complicated answer involves our proprietary software which uses a beat wave algorithm to assess the cue points that we add to each and every song by hand during remastering along with the individual BPM of each song to create a playlist with consistent intensities throughout and seamless transitions between matched songs. </p>
        
        <h2 id="settings">Music Settings</h2>
        <h4>Can I filter out songs with explicit lyrics?</h4>
        <p>Yes, we have different filters that will ensure that your music is safe for little ears and family spaces. We also have filters for our video clips. So if a track that you love has an inappropriate film clip we can make sure that the song plays but the clip doesn’t. </p>
        <h4>Can I ban an artist or song? </h4>
        <p>Yes, your music is completely personalised. You can use Manage My Nightlife (our mobile app) or the web app to remove songs either just once or permanently to prevent them from ever playing again. This data will be viewable by your Nightlife Music team so that we can continue to refine your brief. If you want to remove an artist just let us know. </p>
        
        <h2 id="visuals-da">Visuals and Digital Advertising</h2>
        <h4>Can I schedule and put my own content onto my screens? </h4>
        <p>Yes, our integrated digital advertising platform lets you control everything your customers see. There are inbuilt templates to help you make ads on the fly or you can upload your own content and ads. The web app lets you manage the display times and frequency of your assets so that you can ensure your Friday promos show on Fridays and your Tuesday promos show on Tuesdays. </p>
        <h4>What kind of content is available for visuals?</h4>
        <p>We have clips from around the globe, with something to suit every space. Whether its adrenaline packed zip lining or fashion shoots in Versailles. Check out more about our visuals options here.
        <h4>Do the visuals match up with my music?</h4>
        <p>Because your Nightlife System is completely integrated with music and visuals all managed from the same system everything works together. You will have access to Ambient Visuals, Music Videos and Digital Advertising. You can set your desired timing and frequency for each visual type and then the transitions will work smoothly, synched around the song lengths and music videos.</p>
        
        <h2 id="products">Products</h2>
        <h4>What is the Manage My Nightlife mobile app?</h4>
        <p>Some call it a remote control on steroids, some call it going into “God mode”. Whatever you want to call it, it will help you manage your music and visuals on the go by giving you control over you Nightlife System from your smartphone. </p>
        <h4>What is crowdDJ?</h4>
        <p>CrowdDJ is a beta product that allows the general public to have a say in your venue’s music. It can be used on a standalone tablet in kiosk mode – which makes it like a modern take on the jukebox or individuals can download it onto their own phone and join in. This product is under continuous development based on client feedback. Future functionality is planned to include a payment feature, promotional tie-ins and consumer streaming pairing to let venues share their playlists with their customers. </p>
        <h4>How do I give feedback or suggest new features?</h4>
        <p>We love feedback! The best way to give your feedback is to give your dedicated Relationship Manager a call on 1800 773 468 or drop them an email to let them know. That way the feedback will be directly driven from your account and you’ll be the first to know what’s happening! Other ways you can get in contact include our info@nightlife.com.au email or contact form on our website. </p>
        
        <h2 id="licensing">Public Performance Licensing</h2>
        <h4>What is public performance licensing?</h4>
        <p>Public Performance Licensing refers to the legal obligations that businesses have to obtain permission to play music in a commercial environment. This is different from when you listen to music in a private environment i.e. at home or in the car. </p>
        <h4>Why aren’t my venue’s APRA/PPCA licence fees included in my Nightlife subscription?</h4>
        <p>APRA and PPCA requires each venue to have their own public performance licences because every venue uses music and music videos differently, thus each attracting different licence fees. We cover the licences needed to supply the music, known as mechanical copyright licences, but each venue is responsible for its own public performance licence. </p>
        <h4>Who are APRA|AMCOS and PPCA and why do we have to pay them for the right to publicly perform music?</h4>
        <p>Ownership of music can get very complicated. There’s the composer, the writer, the artist, plus the record company, managers etc. 
          As it would be nearly impossible to seek the permission of every owner/stakeholder within every piece of music used in your business, Performing Rights Organisations make this easier by offering licenses for all the music in their repertoire, and any music that is owned by the same kind of organisation globally. </p>
          <p>These are called blanket licenses. In return for these blanket licenses, Performing Rights Organisations charge a licensing fee based on the way their music is being used. This is then distributed back to their members as royalties.   
          APRA AMCOS are the Performing Rights Organisation in Australia that collect and distribute royalties on behalf of the publishing community i.e. songwriters, composers and music publishers. 
          PPCA is the Performing Rights Organisation in Australia that collect and distribute royalties on behalf of the owner of sound recordings i.e. record companies. </p>
          <p>The easiest way to see this is that APRA|AMCOS members write the music and PPCA members record it. Sometimes the same person or group will write and record a song and they will get royalties from both organisations. </p>
        <h4>What are my licensing requirements?</h4>
        <p>There are three main considerations when using music in a business:</p>
        <div class="well">
            <ol>
                <li>Music you intend to play must be licenced for commercial use – which is what Nightlife supplies (CDs, MP3s, streaming services are for private use only);</li>
                <li>A licence to publicly perform any published music – or an APRA License is required</li>
                <li>A licence to publicly perform any sound recordings – or a PPCA License is required</li>
            </ol>
        </div>
          <p>It can get a little confusing but don’t worry, we have a team full of licensing experts who can help you understand exactly what you need. </p>

          <p>Can't find what you want? <a href="<?php echo $contact; ?>">Contact us</a>.</p>
        </div>
    </div>
</section>