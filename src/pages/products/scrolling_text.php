<section id="mmn" class="row text-center" style="background-image: url('img/what_we_do/products/nightlife_scrolling_text.jpg'); color: white;">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	    <a href="what-we-do#products">
	    	<button class="btn btn-success pull-left"><i class="fa fa-arrow-left"></i> Products &amp; Features</button>
	    </a>	
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-2">
		<div class="col-lg-8 col-lg-offset-2">
		    <i class="fa fa-text-width fa-4x"></i>
		    <h2>Scrolling Text</h2>
		    <h3>Got something to say and need to say it quickly? </h3>
		    <hr class="colored">
		</div>
		<div class="col-lg-8 col-lg-offset-2">
		    <p>Our scrolling text feature lets you run a CNN style news banner across the bottom of your screen and share your messages quickly. You can save and display up to 50 personalised messages.  </p>
		</div>
	</div>
</section>