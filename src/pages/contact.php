<section id="contact">
	<div class="container">
		<h1>Contact</h1>
		<div class="col-xs-12 col-sm-4">
			<h3><i class="fa fa-phone"></i> Call Us</h3>
			<p>Enquiries: 1800 679 748</p>
			<p>Service: 1800 773 468</p>
	 		
	 		<h3>Nightlife HQ</h3>
	 		<p>50 Cribb St, Milton </p>
			<p>QLD Australia 4064</p>
	 	
	 	<h3>Email</h3>
		<p>info@nightlife.com.au</p>
		</div>
		<div class="col-xs-12 col-sm-8">
			<iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=Nightlife%20Music%2C%20Milton%2C%20Queensland%2C%20Australia&key=AIzaSyACqlXthmrovcK5mmAmFTPJuvryzMQbsiQ"></iframe>
		</div>
	</div>
</section>
<section>
	<div class="container">
		<?php include_once('pages/components/contact_form.php'); ?>
	</div>
</section>