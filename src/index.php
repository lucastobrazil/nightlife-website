<?php
    // NIGHTLIFE INDEX.PHP FILE
    // Made up of the following components
    include('head.php'); 
    include('nav.php'); 

    // Include page. 
    // There is an array of $allowedPages set in head.php 
    // If the $page var set is not allowed, we display the 404 page
    if((@include  isset($allowedPages[$page]) ? $allowedPages[$page] : $allowedPages["page404"]) === false)
    {
        // handle error
        include_once($allowedPages["page404"]); 
    }else{
        include_once( isset($allowedPages[$page]) ? $allowedPages[$page] : $allowedPages["page404"] );     
    }
    include('footer.php'); 
?>