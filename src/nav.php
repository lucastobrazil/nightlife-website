 <?php
    
    $homeLink = '';
    
    if($page === 'home'){
        $homeLink = '#page-top';
    }else{
        $homeLink = $home;
    }

    $subMenuShouldScroll = false; //default

    if($page === 'what-we-do'){$subMenuShouldScroll = true;}

 ?>

 <!-- Navigation -->
<!-- Note: navbar-default and navbar-inverse are both supported with this theme. -->
<nav class="navbar navbar-inverse navbar-fixed-top <?php if($page === 'home'){ echo 'navbar-expanded';}?>">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll <?php if($page==='home' && !isMobile()) echo 'white';?>" href="<?php echo $homeLink; ?>">
                <!-- Small trick so we can manipulate the colours of SVG inline -->
                <!-- use file_get_contents to render the SVG data in the HTML -->
                <?php echo file_get_contents("img/svg/nightlife-logo-svg.svg"); ?>
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li class="dropdown <?php if($page === 'what-we-do'){echo 'active callout-bottom ';}?>">
                    <a href="<?php echo $what_we_do; ?>"  class="dropdown-toggle" data-hover="dropdown" aria-expanded="true">What We Do</a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="What-We-Do-dropdown">
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'what-we-do'){echo 'page-scroll';}?>" tabindex="-1" href="<?php if($page != 'what-we-do'){echo $what_we_do;}?>#music">
                                <i class="fa fa-music"></i> Music
                            </a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'what-we-do'){echo 'page-scroll';}?>" tabindex="-1" href="<?php if($page != 'what-we-do'){echo $what_we_do;}?>#visuals" data-target="visuals">
                                <i class="fa fa-film"></i> Visuals
                            </a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'what-we-do'){echo 'page-scroll';}?>" tabindex="-1" href="<?php if($page != 'what-we-do'){echo $what_we_do;}?>#digitalsignage">
                                <i class="fa fa-bullhorn"></i> Digital Signage
                            </a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'what-we-do'){echo 'page-scroll';}?>" tabindex="-1" href="<?php if($page != 'what-we-do'){echo $what_we_do;}?>#licensing">
                                <i class="fa fa-usd"></i> Licensing
                            </a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'what-we-do'){echo 'page-scroll';}?>" tabindex="-1" href="<?php if($page != 'what-we-do'){echo $what_we_do;}?>#products">
                                <i class="fa fa-leaf"></i> Products &amp; Features
                            </a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'what-we-do'){echo 'page-scroll';}?>" tabindex="-1" href="<?php echo $faq;?>">
                                <i class="fa fa-comment"></i> FAQ
                            </a>
                        </li>
                      </ul>
                </li>
                <li class="dropdown <?php if($page === 'case-studies'){echo 'active callout-bottom ';}?>">
                    <a href="<?php echo $case_studies; ?>" class="dropdown-toggle" data-hover="dropdown" aria-expanded="true">Case Studies</a>
                    <!-- Left out this dropdown until we have more case studies -->
                    <!--<ul class="dropdown-menu" role="menu" aria-labelledby="Case-Studies-dropdown">
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'case-studies'){echo 'page-scroll';}?>" tabindex="-1" href="<?php if($page != 'case-studies'){echo $case_studies;}?>#gyms">
                                <i class="fa fa-usd"></i> Gyms
                            </a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'case-studies'){echo 'page-scroll';}?>" tabindex="-1" href="<?php if($page != 'case-studies'){echo $case_studies;}?>#bars">
                                <i class="fa fa-usd"></i> Bars
                            </a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'case-studies'){echo 'page-scroll';}?>" tabindex="-1" href="<?php if($page != 'case-studies'){echo $case_studies;}?>#clubs">
                                <i class="fa fa-usd"></i> Clubs
                            </a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'case-studies'){echo 'page-scroll';}?>" tabindex="-1" href="<?php if($page != 'case-studies'){echo $case_studies;}?>#leisure">
                                <i class="fa fa-usd"></i> Leisure
                            </a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'case-studies'){echo 'page-scroll';}?>" tabindex="-1" href="<?php if($page != 'case-studies'){echo $case_studies;}?>#casinos">
                                <i class="fa fa-usd"></i> Casinos
                            </a>
                        </li>                                                                                                
                    </ul>
                -->
                </li>
                <li class="dropdown <?php if($page === 'our-story' || $page === 'careers'){echo 'active callout-bottom ';}?>">
                    <a href="<?php echo $our_story; ?>" class="dropdown-toggle" data-hover="dropdown" aria-expanded="true">Our Story</a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="our-story-dropdown">
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'our-story'){echo 'page-scroll';}?>" tabindex="-1" href="<?php if($page != 'our-story'){echo $our_story;}?>#community">
                                 <i class="fa fa-building-o"></i> Community
                             </a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem"tabindex="-1" href="<?php if($page != 'careers'){echo $careers;}?>">
                                 <i class="fa fa-briefcase"></i> Careers
                            </a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'our-story'){echo 'page-scroll';}?>" tabindex="-1" href="<?php echo $faq;?>">
                                 <i class="fa fa-comment"></i> FAQ
                            </a>
                        </li>
                      </ul>
                </li>
                <li class="dropdown <?php if($page === 'latest'){echo 'active callout-bottom';}?>">
                    <a href="../blog" class="dropdown-toggle" data-hover="dropdown" aria-expanded="true">Latest</a>
                      <!--
                      <ul class="dropdown-menu" role="menu" aria-labelledby="Latest-dropdown">
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'latest'){echo 'page-scroll';}?>" tabindex="-1" href="../blog">
                                <i class="fa fa-briefcase"></i> Blog
                            </a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'latest'){echo 'page-scroll';}?>" tabindex="-1" href="<?php if($page != 'latest'){echo $latest;}?>#media">
                                <i class="fa fa-certificate"></i> Media
                            </a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem" class="<?php if($page === 'latest'){echo 'page-scroll';}?>" tabindex="-1" href="<?php if($page != 'latest'){echo $latest;}?>#faq">
                                <i class="fa fa-briefcase"></i> Resources
                            </a>
                        </li>
                      </ul>
                  -->
                </li>
                <li class="<?php if($page === 'login'){echo 'active callout-bottom';}?>">
                    <a  href="<?php echo $login; ?>">Login</a>
                </li>                    
                <li class="<?php if($page === 'contact'){echo 'active callout-bottom';}?>">
                    <a href="<?php echo $contact; ?>">Contact</a>
                </li>
                <li>
                    <a id="label"><i class="fa fa-search" id="search-label"></i></a>
                    <form id="search" action="#" method="post">
                        <div id="input"><input type="text" name="search-terms" class="st-default-search-input" id="search-terms" placeholder="Search Site..." /></div>
                    </form>
                </li>                    
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
<?php
    if($page === 'home'){
        echo "<script>showJourneyNav = false;</script>";
    }else{
        echo "<script>showJourneyNav = false;</script>";
    }
?>