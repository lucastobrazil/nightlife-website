<section id="about_us">
	<div class="container">
			<h1 class="col-xs-12">Our Story</h1>
			<h3 class="col-xs-12">A DJ and an Engineer walk into a bar...</h3>
		<div class="col-xs-12 col-sm-6">
			<p>And that’s basically how Nightlife Music began; two friends, determined to give venues music that was designed just for them without having to have a DJ permanently on staff. </p>
			<p>What started over 26 years ago as two friends with a whole bunch of mixtapes has now become Australia’s leading commercial music and entertainment supplier. With over 100 staff and cutting edge integration technology Nightlife now serves over 2700 clients Australia-wide plus hundreds more overseas. </p>
		</div>
		<div class="col-xs-12 col-sm-6">
			<img src="img/nightlife_music_hq_brisbane.jpg" class="img-responsive" alt="Nightlife Headquarters; Milton, QLD, Australia" />
		</div>
			<p class="col-xs-12">Music and visuals are an expression of identity and Nightlife makes it possible to deliver this commercially. That’s why we say Nightlife Music is the perfect mix of art and science. </p>
		<div id="community" class="col-xs-12">
			<h1>Community</h1>
			<p>We like to share the love and we are actively involved in several community causes both within Australia and internationally. These charities perform exceptional work and benefit those in need - we are proud to support them.</p>
			<div class="well">
			<div class="row">
				<a href="http://www.worldvision.com.au/" target="_blank">
					<img class="col-xs-12 col-sm-1 thumbnail img-responsive" src="img/world_vision_logo.jpg" />
				</a>
				<p class="col-xs-12 col-sm-11"><a href="http://www.worldvision.com.au/" target="_blank">World Vision</a> - provides relief in emergency situations and works on long-term community development projects.</p>
			</div>
			<div class="row">
				<a href="http://www.msf.org.au" target="_blank">
					<img class="col-xs-12 col-sm-1 thumbnail img-responsive" src="img/doctors_without_borders.png" />
				</a>
				<p  class="col-xs-12 col-sm-11"><a href="http://www.msf.org.au" target="_blank">Medecins Sans Frontieres/Doctors Without Borders</a> - works in nearly 70 countries providing medical aid to those most in need regardless of their race, religion, or political affiliation.</p>
			</div>			
			<div class="row">
				<a href="http://www.rspca.org.au" target="_blank">
					<img class="col-xs-12 col-sm-1 thumbnail img-responsive" src="img/rspca_logo.jpg" />
				</a>
				<p  class="col-xs-12 col-sm-11"><a href="http://www.rspca.org.au" target="_blank">RSPCA</a> - prevents cruelty to animals by actively promoting their care and protection as well as operating animal care and adoption facilities. </p>
			</div>
			</div>

			<h3>ECOLOGICAL SUSTAINABILITY</h3>
			<p>We are always doing our best to reduce our impact on the environment and increase our sustainability. Responsible use of resources is something that is threaded throughout our staff and operations. Tour Nightlife and you’ll find: </p>
			<ul>
				<li>Solar panels </li>
				<li>An electric car and charging station</li>
				<li>Recycling stations in every department</li>
				<li>Staff reading from the screens</li>
				<li>Recycled printing</li>
				<li>Component recycling</li>
				<li>Bicycle parking</li>
			</ul>
		</div>
	</div>
</section>