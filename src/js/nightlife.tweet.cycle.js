/****************************/
/* nightlife.tweet.cycle.js */
/****************************/
jQuery(document).ready(function() {
	// First, check that there's some slides to cycle through
	if(jQuery('#tweet-slides span').length){
		jQuery('#tweet-slides').cycle({ 
			fx: 'scrollLeft', 
			timeout: 8000, 
			speed: 200,
			next: '.next',
			prev: '.prev',
			pagerAnchorBuilder: function(index, el) {
				return '<a href="#">-</a>'; // whatever markup you want
			}
		});
	}
});
